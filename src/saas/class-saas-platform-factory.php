<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Auth_Platform_Factory
 */
class WPDesk_Flexible_Shipping_SaaS_Auth_Platform_Factory {

	const SAAS_PLATFORM_LOGGER_SOURCE = 'saas-platform';

	const ONE_HOUR = 3600;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Auth_Platform_Factory constructor.
	 *
	 * @param \Psr\Log\LoggerInterface $logger Logger.
	 */
	public function __construct( \Psr\Log\LoggerInterface $logger ) {
		$this->logger = new WPDesk_Flexible_Shipping_WooCommerce_Context_Logger(
			$logger,
			self::SAAS_PLATFORM_LOGGER_SOURCE
		);
	}

	/**
	 * Get platform.
	 *
	 * @return \WPDesk\SaasPlatformClient\Platform
	 */
	public function get_platform() {
		$default_headers = array();
		if ( defined( 'FLEXIBLE_SHIPPING_VERSION' ) ) {
			$default_headers['X-Plugin-Version'] = FLEXIBLE_SHIPPING_VERSION;
		}
		$platform_options = new \WPDesk\SaasPlatformClient\PlatformFactoryWordpressOptions();
		$platform_options->setLogger( $this->logger );
		$platform_options->setDefaultRequestHeaders( $default_headers );

		$cache_settings = new WPDesk_Flexible_Shipping_SaaS_Cache_Settings();
		if ( $cache_settings->is_enabled() ) {
			$platform_options->setCachedClient( true );
		}
		$platform_options->setCacheTtl( self::ONE_HOUR );
		$platform = \WPDesk\SaasPlatformClient\PlatformFactory::createPlatformApi( $platform_options );

		return $platform;
	}
}

