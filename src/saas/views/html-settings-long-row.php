<?php /** @var $data array */ ?>
<tr valign="top" class="<?php echo esc_attr( $data['class'] ); ?>">
	<td scope="row" class="titledesc" colspan="2">
		<div><?php echo $data['content']; ?></div>
	</td>
</tr>
