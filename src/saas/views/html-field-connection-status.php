<tr valign="top">
	<th scope="row" class="titledesc">
		<label for="<?php echo $field_id; ?>"><?php echo wp_kses_post( $data['title'] ); ?> <?php echo $tooltip_html; ?></label>
	</th>
	<td class="forminp">
		<fieldset>
			<legend class="screen-reader-text"><span><?php echo wp_kses_post( $title ); ?></span></legend>
			<span id="<?php echo $field_id; ?>"><?php echo $status_html; ?></span>
			<?php echo $description_html; ?>
		</fieldset>
		<?php if ( $ajax_action ) : ?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					var data = {
						'action'     : '<?php echo $ajax_action; ?>',
						'nonce'      : '<?php echo $ajax_nonce; ?>',
						'service_id' : <?php echo $service_id; ?>,
					};
					var status_span = jQuery('#<?php echo $field_id; ?>');
					jQuery.post(ajaxurl, data, function( response ) {
						status_span.html(response.message);
						if ( response.status == '<?php echo WPDesk_Flexible_Shipping_SaaS_Connection_Status_Ajax::STATUS_OK; ?>' ) {
							status_span.css('color', 'green');
						}
						else {
							status_span.css('color', 'red');
						}
					}).fail(function() {
						status_span.css('color', 'red');
						status_span.html('<?php echo $ajax_error_message; ?>');
					});
				})
			</script>
		<?php endif; ?>
	</td>
</tr>
