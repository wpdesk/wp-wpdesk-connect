<?php wp_nonce_field( WPDesk_Flexible_Shipping_SaaS_Connection::NONCE_CONNECTION_ACTION, WPDesk_Flexible_Shipping_SaaS_Connection::NONCE_CONNECTION_NAME ); ?>
<input type="hidden" id="<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::SUBMIT_CONNECTION_PARAMETER; ?>" name="<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::SUBMIT_CONNECTION_PARAMETER; ?>" value="0" />
<table class="form-table">
	<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="api_key"><?php _e( 'Connect Key', 'flexible-shipping' ); ?></label>
		</th>
		<td class="forminp">
			<input class="input-text regular-input " type="text" name="api_key" id="api_key" value="<?php echo $api_key ?>" <?php echo $readonly;?> />
		</td>
	</tr>
</table>

<p class="flexible-shipping-connect-submit">
	<input type="submit" id="<?php echo $button_id; ?>" class="button-primary woocommerce-save-button" value="<?php echo $submit_button_value; ?>" />
	<?php ?>
</p>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::BUTTON_ID_DISCONNECT; ?>').click(function(e){
			if ( !confirm( '<?php _e( 'Are you sure you want do disconnect your store from Flexible Shipping Connect?\n\nAll shipping services will be disabled.', 'flexible-shipping' ); ?>' ) ) {
				e.preventDefault();
				return false;
			}
			else {
				jQuery('#<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::SUBMIT_CONNECTION_PARAMETER; ?>').val('1');
			}
		});
		function api_key_key_up() {
			var button_connect = jQuery('#<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::BUTTON_ID_CONNECT; ?>');
			if ( jQuery('#api_key').val() != '' ) {
				button_connect.prop('disabled', false);
			}
			else {
				button_connect.prop('disabled', true);
			}
		}
		api_key_key_up();
		jQuery('#api_key').keyup(function(){
			api_key_key_up();
		})
		jQuery('#api_key').change(function(){
			api_key_key_up();
		})
		jQuery('#<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::BUTTON_ID_CONNECT; ?>').click(function(){
			jQuery('#<?php echo WPDesk_Flexible_Shipping_SaaS_Connection::SUBMIT_CONNECTION_PARAMETER; ?>').val('1');
		});
	})
</script>
