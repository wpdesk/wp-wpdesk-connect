<?php
if( !$saas_connected ):
	ob_start();

	echo '<p>' . __( 'Welcome to new Flexible Shipping Connect installation guide.', 'flexible-shipping' ) . '</p>';

	echo '<p><strong>' . __( 'To complete the installation please click the Sign up for Connect button below.', 'flexible-shipping' ) . '</strong></p>';

	echo '<p>' . sprintf( __( 'The %saccount is free (no credit card needed)%s and allows you to send packages directly from here!', 'flexible-shipping' ), '<strong>',  '</strong>' ) . '</p>';

	echo '<p>' . __( 'If you want to learn more about other benefits, you may click one of the tabs on the left.', 'flexible-shipping' ) . '</p>';

	$fs_description = ob_get_contents();
	ob_end_clean();

	$fs_content = array(
		array(
			'title'       => 'Flexible Shipping',
			'heading'     => __( 'Ship your orders faster with Flexible Shipping Connect', 'flexible-shipping' ),
			'description' => $fs_description,
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6zm-16.2 55.1l-352 208C45.6 483.9 32 476.6 32 464V47.9c0-16.3 16.4-18.4 24.1-13.8l352 208.1c10.5 6.2 10.5 21.4.1 27.6z"/></svg>'
		)
	);

	$couriers = array(
		array(
			'title'   => 'UPS',
			'country' => array(),
			'label'   => '',
			'logo'    => 'https://s3.eu-central-1.amazonaws.com/wpdeskcdn/couriers/ups.svg'
		),
		array(
			'title'   => 'FedEx',
			'country' => array(),
			'label'   => __( 'soon!', 'flexible-shipping' ),
			'logo'    => 'https://s3.eu-central-1.amazonaws.com/wpdeskcdn/couriers/fedex.svg'
		),
		array(
			'title'   => 'DPD UK',
			'country' => array( 'GB' ),
			'label'   => __( 'soon!', 'flexible-shipping' ),
			'logo'    => 'https://s3.eu-central-1.amazonaws.com/wpdeskcdn/couriers/dpd.svg'
		),
		array(
			'title'   => 'Royal Mail',
			'country' => array( 'GB' ),
			'label'   => __( 'soon!', 'flexible-shipping' ),
			'logo'    => 'https://s3.eu-central-1.amazonaws.com/wpdeskcdn/couriers/royal-mail.png'
		)
	);

	$base_location = wc_get_base_location();

	ob_start();

	_e( 'Using Flexible Shipping Connect, you can send packages via:', 'flexible-shipping' );

	echo '<div class="fs-couriers">';

	foreach ( $couriers as $key => $value ) :

		if ( empty( $value['country'] ) || in_array( $base_location['country'], $value['country'] ) ) :
			echo '<div class="fs-courier">';

			echo '<img src="' . $value['logo'] . '">';

			if ( $value['label'] )
				echo '<span class="fs-courier-label">' . $value['label'] . '</span>';

			echo '</div>';
		endif;

	endforeach;

	echo '</div>';

	echo '<a href="https://flexibleshipping.com/product-category/integrations/?utm_campaign=flexible-shipping&utm_source=user-site&utm_medium=link&utm_term=possible-integrations&utm_content=fs-notice-integrations" target="_blank">' . __( 'Read more about possible integrations &rarr;', 'flexible-shipping') . '</a>';

	$couriers_description = ob_get_contents();
	ob_end_clean();

	$boxes_content = array(
		array(
			'title'       => __( 'Create & Print Labels', 'flexible-shipping' ),
			'heading'     => __( 'Create & Print Labels', 'flexible-shipping' ),
			'description' => __( 'Create shipments with predefined services, sizes and weights automatically filled in from order items. Download printable PDF shipping labels directly from WooCommerce orders list in bulk or one by one from order details.', 'flexible-shipping' ),
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M280 96h-16c-4.4 0-8 3.6-8 8v304c0 4.4 3.6 8 8 8h16c4.4 0 8-3.6 8-8V104c0-4.4-3.6-8-8-8zm-64 0h-16c-4.4 0-8 3.6-8 8v304c0 4.4 3.6 8 8 8h16c4.4 0 8-3.6 8-8V104c0-4.4-3.6-8-8-8zM592 0H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h544c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zm16 464c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V48c0-8.8 7.2-16 16-16h544c8.8 0 16 7.2 16 16v416zM152 96h-48c-4.4 0-8 3.6-8 8v304c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V104c0-4.4-3.6-8-8-8zm384 0h-48c-4.4 0-8 3.6-8 8v304c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V104c0-4.4-3.6-8-8-8zm-128 0h-48c-4.4 0-8 3.6-8 8v304c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V104c0-4.4-3.6-8-8-8z"/></svg>'
		),
		array(
			'title'       => __( 'Calculate Rates', 'flexible-shipping' ),
			'heading'     => __( 'Calculate Rates', 'flexible-shipping' ),
			'description' => __( 'Calculate shipping rates in real time, and get available service types and expected delivery time based on customer\'s address.', 'flexible-shipping' ),
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M80 448h288c8.84 0 16-7.16 16-16V240c0-8.84-7.16-16-16-16H80c-8.84 0-16 7.16-16 16v192c0 8.84 7.16 16 16 16zm208-96v-96h64v160h-64v-64zm-96-96h64v64h-64v-64zm0 96h64v64h-64v-64zm-96-96h64v64H96v-64zm0 96h64v64H96v-64zM400 0H48C22.4 0 0 22.4 0 48v416c0 25.6 22.4 48 48 48h352c25.6 0 48-22.4 48-48V48c0-25.6-22.4-48-48-48zm16 464c0 7.93-8.07 16-16 16H48c-7.93 0-16-8.07-16-16V192h384v272zm0-304H32V48c0-7.93 8.07-16 16-16h352c7.93 0 16 8.07 16 16v112z"/></svg>'
		),
		array(
			'title'       => __( 'Track Parcels', 'flexible-shipping' ),
			'heading'     => __( 'Track Parcels', 'flexible-shipping' ),
			'description' => __( 'Always keep your customers (and yourself) up to date with automatic confirmation emails with tracking link and delivery status.', 'flexible-shipping' ),
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M208 96c-49.53 0-89.81 40.3-89.81 89.83 0 34.66 38.34 87.2 70.53 125.2 4.81 5.7 11.84 8.97 19.28 8.97s14.47-3.27 19.31-8.97c32.16-38 70.5-90.55 70.5-125.2C297.81 136.3 257.53 96 208 96zm0 188.23c-35.31-42.7-57.81-80.89-57.81-98.41 0-31.89 25.94-57.83 57.81-57.83s57.81 25.94 57.81 57.83c0 17.52-22.5 55.71-57.81 98.41zM208 160c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zm300.47 321.58l-128.99-129c-2.3-2.3-5.3-3.5-8.5-3.5h-10.3c34.3-37.1 55.3-86.6 55.3-141.09C415.98 93.1 322.88 0 207.99 0S0 93.1 0 207.99c0 114.89 93.1 207.99 207.99 207.99 54.5 0 103.99-21 141.09-55.2v10.2c0 3.2 1.3 6.2 3.5 8.5l128.99 128.99c4.7 4.7 12.3 4.7 17 0l9.9-9.9c4.71-4.69 4.71-12.29 0-16.99zm-300.48-97.6C110.7 383.98 32 305.29 32 207.99 32 110.69 110.7 32 207.99 32s175.99 78.7 175.99 175.99c0 97.3-78.69 175.99-175.99 175.99z"/></svg>'
		),
		array(
			'title'       => __( 'Automate Shipping', 'flexible-shipping' ),
			'heading'     => __( 'Automate Shipping', 'flexible-shipping' ),
			'description' => __( 'Automatically create shipments and print labels based on order status. Change order status after shipment creation.', 'flexible-shipping' ),
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M48 416c-8.82 0-16-7.18-16-16V256h160v40c0 13.25 10.75 24 24 24h80c13.25 0 24-10.75 24-24v-40h40.23c10.06-12.19 21.81-22.9 34.77-32H32v-80c0-8.82 7.18-16 16-16h416c8.82 0 16 7.18 16 16v48.81c5.28-.48 10.6-.81 16-.81s10.72.33 16 .81V144c0-26.51-21.49-48-48-48H352V24c0-13.26-10.74-24-24-24H184c-13.26 0-24 10.74-24 24v72H48c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h291.37a174.574 174.574 0 0 1-12.57-32H48zm176-160h64v32h-64v-32zM192 32h128v64H192V32zm358.29 320H512v-54.29c0-5.34-4.37-9.71-9.71-9.71h-12.57c-5.34 0-9.71 4.37-9.71 9.71v76.57c0 5.34 4.37 9.71 9.71 9.71h60.57c5.34 0 9.71-4.37 9.71-9.71v-12.57c0-5.34-4.37-9.71-9.71-9.71zM496 224c-79.59 0-144 64.41-144 144s64.41 144 144 144 144-64.41 144-144-64.41-144-144-144zm0 256c-61.76 0-112-50.24-112-112s50.24-112 112-112 112 50.24 112 112-50.24 112-112 112z"/></svg>'
		),
		array(
			'title'       => __( 'Manage Orders', 'flexible-shipping' ),
			'heading'     => __( 'Manage Orders', 'flexible-shipping' ),
			'description' => __( 'Get a complete overview of your orders and shipments in your WooCommerce dashboard with shipments statuses and filters.', 'flexible-shipping' ),
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M112 256h352c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16H112c-8.8 0-16 7.2-16 16v224c0 8.8 7.2 16 16 16zM256 32h64v76.2l-32-16-32 16V32zm-128 0h96v128l64-32 64 32V32h96v192H128V32zm430.3 301.6c-9.6-8.6-22.1-13.4-35.2-13.4-12.5 0-24.8 4.3-34.6 12.2l-61.6 49.3c-1.9 1.5-4.2 2.3-6.7 2.3h-41.6c4.6-9.6 6.5-20.7 4.8-32.3-4-27.9-29.6-47.7-57.8-47.7H181.3c-20.8 0-41 6.7-57.6 19.2L85.3 352H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h88l46.9-35.2c11.1-8.3 24.6-12.8 38.4-12.8H328c13.3 0 24 10.7 24 24s-10.7 24-24 24h-88c-8.8 0-16 7.2-16 16s7.2 16 16 16h180.2c9.7 0 19.1-3.3 26.7-9.3l61.6-49.2c4.2-3.4 9.5-5.2 14.6-5.2 5 0 9.9 1.7 13.8 5.2 10.1 9.1 9.3 24.5-.9 32.6l-100.8 80.7c-7.6 6.1-17 9.3-26.7 9.3H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h400.5c17 0 33.4-5.8 46.6-16.4L556 415c12.2-9.8 19.5-24.4 20-40s-6-30.8-17.7-41.4z"/></svg>'
		),
		array(
			'title'       => __( 'Couriers', 'flexible-shipping' ),
			'heading'     => __( 'Couriers', 'flexible-shipping' ),
			'description' => $couriers_description,
			'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M632 384h-24V275.9c0-16.8-6.8-33.3-18.8-45.2l-83.9-83.9c-11.8-12-28.3-18.8-45.2-18.8H416V78.6c0-25.7-22.2-46.6-49.4-46.6H49.4C22.2 32 0 52.9 0 78.6v290.8C0 395.1 22.2 416 49.4 416h16.2c-1.1 5.2-1.6 10.5-1.6 16 0 44.2 35.8 80 80 80s80-35.8 80-80c0-5.5-.6-10.8-1.6-16h195.2c-1.1 5.2-1.6 10.5-1.6 16 0 44.2 35.8 80 80 80s80-35.8 80-80c0-5.5-.6-10.8-1.6-16H632c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zM460.1 160c8.4 0 16.7 3.4 22.6 9.4l83.9 83.9c.8.8 1.1 1.9 1.8 2.8H416v-96h44.1zM144 480c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm63.6-96C193 364.7 170 352 144 352s-49 12.7-63.6 32h-31c-9.6 0-17.4-6.5-17.4-14.6V78.6C32 70.5 39.8 64 49.4 64h317.2c9.6 0 17.4 6.5 17.4 14.6V384H207.6zM496 480c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm0-128c-26.1 0-49 12.7-63.6 32H416v-96h160v96h-16.4c-14.6-19.3-37.5-32-63.6-32z"/></svg>'
		)
	);

	$content = array_merge( $fs_content, $boxes_content );
?>
<div class="fs-connect__inner-container">
	<div class="fs-connect__vertical-nav">
		<div class="fs-connect__vertical-nav-container">
			<?php
				$count = 1;

				foreach ( $content as $key => $value ) :
			?>
				<div class="vertical-menu__feature-item <?php if ( $count === 1 ) : ?>vertical-menu__feature-item-is-selected<?php endif; ?>">
					<div class="vertical-menu__feature-item-icon">
						<?php echo $value['icon']; ?>
					</div>

					<span class="vertical-menu__feature-item-label"><?php echo $value['title']; ?></span>
				</div>
			<?php
				$count++;

				endforeach;
			?>
		</div>
	</div>

	<div class="fs-connect__content-container">
		<?php
			$count = 1;

			foreach ( $content as $key => $value ) :
		?>
			<div class="fs-connect__slide <?php if ( $count === 1 ) : ?>fs__slide-is-active<?php endif; ?>">
				<h2><?php echo $value['heading']; ?></h2>

				<?php if ( $count != 7 ) : ?>
					<div class="fs-connect__content-icon fs-connect-illo">
						<?php echo $value['icon']; ?>
					</div>
				<?php endif; ?>

				<p><?php echo $value['description']; ?></p>

				<p class="fs-banner__button-container">
					<a class="button button-primary button-large" href="<?php echo add_query_arg( 'page', WPDesk_Flexible_Shipping_SaaS_User_Registration::REGISTER_PAGE_SLUG, admin_url( 'admin.php' ) ); ?>"><?php _e( 'Sign up for Connect', 'flexible-shipping' ); ?></a>

					<?php $button_text = $count === 1 ? __( 'Start quick tour', 'flexible-shipping' ) : __( 'Next feature', 'flexible-shipping' ); ?>

					<?php if ( $count != 7 ) : ?>
						<a class="button next-feature" href="#"><?php echo $button_text; ?></a>
					<?php endif; ?>
				</p>
			</div>
		<?php
			$count++;

			endforeach;
		?>
	</div>
</div>
<?php endif; ?>