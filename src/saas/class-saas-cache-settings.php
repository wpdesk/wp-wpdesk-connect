<?php

/**
 * Handles cache settings
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Cache_Settings
 */
class WPDesk_Flexible_Shipping_SaaS_Cache_Settings {

	const SETTINGS_OPTION_NAME = 'disable_cache';

	const OPTION_NAME = 'fs_disable_cache';

	const OPTION_VALUE_DISABLED = '0';
	const OPTION_VALUE_ENABLED  = '1';

	/**
	 * Enabled.
	 *
	 * @var bool
	 */
	private $enabled = false;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Logger_Settings constructor.
	 */
	public function __construct() {
		$option_value  = get_option( self::OPTION_NAME, self::OPTION_VALUE_ENABLED );
		$this->enabled = self::OPTION_VALUE_ENABLED === $option_value;
	}

	/**
	 * Is enabled.
	 *
	 * @return bool
	 */
	public function is_enabled() {
		return $this->enabled;
	}

	/**
	 * Update option from saas settings.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Settings $saas_settings Saas settings.
	 */
	public function update_option_from_saas_settings( WPDesk_Flexible_Shipping_SaaS_Settings $saas_settings ) {
		$saas_settings_value = $saas_settings->get_option( self::SETTINGS_OPTION_NAME );
		if ( ! empty( $saas_settings_value ) && 'yes' === $saas_settings_value ) {
			$option_value = self::OPTION_VALUE_ENABLED;
		} else {
			$option_value = self::OPTION_VALUE_DISABLED;
		}
		update_option( self::OPTION_NAME, $option_value );
		$this->enabled = self::OPTION_VALUE_ENABLED === $option_value;
	}

	/**
	 * Add fields to settings.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	public function add_fields_to_settings( array $settings ) {
		$settings[ self::SETTINGS_OPTION_NAME ] = array(
			'type'        => 'checkbox',
			'label'       => __( 'Enable', 'flexible-shipping' ),
			'title'       => __( 'Cache', 'flexible-shipping' ),
			'description' => __( 'Enables Flexible Shipping Connect cache. Disable only for debugging.', 'flexible-shipping' ),
			'default'     => 'yes',
		);
		return $settings;
	}

}
