<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings_Connection
 */
class WPDesk_Flexible_Shipping_SaaS_Connection_Status_Ajax implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const AJAX_ACTION = 'wpdesk_fs_connection_status';
	const NONCE_NAME  = 'status_field_nonce';

	const STATUS_ERROR = 'ERROR';
	const STATUS_OK    = 'OK';

	const RESPONSE_MESSAGE = 'message';
	const RESPONSE_STATUS  = 'status';


	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Connection_Status_Ajax constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection SaaS connection.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection
	) {
		$this->saas_connection = $saas_connection;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_' . self::AJAX_ACTION, array( $this, 'handle_ajax_request' ) );
	}

	/**
	 * Prepare response.
	 *
	 * @param \WPDesk\SaasPlatformClient\Response\ShippingServicesSettings\Test\GetSettingsTestResponse $test_response Test connection response.
	 *
	 * @return array
	 */
	public function prepare_response( $test_response ) {
		$response = array(
			self::RESPONSE_STATUS  => self::STATUS_ERROR,
			self::RESPONSE_MESSAGE => __( 'Something go wrong!', 'flexible-shipping' ),
		);
		try {
			if ( $test_response->isError() ) {
				$response[ self::RESPONSE_MESSAGE ] = sprintf(
					// Translators: response code.
					__( 'Invalid response from Flexible Shipping Connect platform. Response code: %1$s.', 'flexible-shipping' ),
					$test_response->getResponseCode()
				);
			} else {
				$response[ self::RESPONSE_STATUS ] = $test_response->getStatus();
				if ( self::STATUS_ERROR === $response[ self::RESPONSE_STATUS ] ) {
					$response[ self::RESPONSE_MESSAGE ] = $test_response->getMessage();
				} else {
					$response[ self::RESPONSE_MESSAGE ] = __( 'OK', 'flexible-shipping' );
				}
			}
		} catch ( Exception $e ) {
			$response[ self::RESPONSE_STATUS ]  = self::STATUS_ERROR;
			$response[ self::RESPONSE_MESSAGE ] = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Handle AJAX request.
	 */
	public function handle_ajax_request() {
		check_ajax_referer( self::NONCE_NAME, 'nonce' );

		$service_id = $_REQUEST['service_id'];

		$test_response = $this->saas_connection->get_platform()->requestGetSettingsTest( $service_id );

		wp_send_json( $this->prepare_response( $test_response ) );
		exit;
	}

}
