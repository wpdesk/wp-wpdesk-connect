<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Country_State_Field
 */
class WPDesk_Flexible_Shipping_SaaS_Country_State_Field {

	const DELIMITER = ':';

	const COUNTRY_STATE_CODE_FIELD = 'country_state_code';
	const COUNTRY_CODE_FIELD       = 'country_code';
	const STATE_CODE_FIELD         = 'state_code';

	/**
	 * Country code.
	 *
	 * @var string|null
	 */
	private $country_code;

	/**
	 * State code.
	 *
	 * @var string|null
	 */
	private $state_code;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Country_State_Field constructor.
	 *
	 * @param string|null $country_code Country code.
	 * @param string|null $state_code State code.
	 */
	public function __construct( $country_code, $state_code ) {
		$this->country_code = $country_code;
		$this->state_code   = $state_code;
	}

	/**
	 * Create from single field.
	 *
	 * @param string $country_state_code Country state code.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Country_State_Field
	 */
	public static function create_from_single_field( $country_state_code ) {
		$country_state_code_array = explode( self::DELIMITER, $country_state_code );
		return new WPDesk_Flexible_Shipping_SaaS_Country_State_Field(
			$country_state_code_array[0],
			isset( $country_state_code_array[1] ) ? $country_state_code_array[1] : null
		);
	}

	/**
	 * Create from serialized settings.
	 *
	 * @param array $settings Serialized settings.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Country_State_Field
	 */
	public static function create_from_serialized_settings( array $settings ) {
		if ( ! empty( $settings[ self::COUNTRY_STATE_CODE_FIELD ] ) ) {
			return self::create_from_single_field( $settings[ self::COUNTRY_STATE_CODE_FIELD ] );
		} else {
			return new WPDesk_Flexible_Shipping_SaaS_Country_State_Field( null, null );
		}
	}

	/**
	 * Create from serialized settings.
	 *
	 * @param array $settings Serialized settings.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Country_State_Field
	 */
	public static function create_from_settings( array $settings ) {
		if ( ! empty( $settings[ self::COUNTRY_CODE_FIELD ] ) ) {
			return new WPDesk_Flexible_Shipping_SaaS_Country_State_Field(
				$settings[ self::COUNTRY_CODE_FIELD ],
				isset( $settings[ self::STATE_CODE_FIELD ] ) ? $settings[ self::STATE_CODE_FIELD ] : null
			);
		} else {
			return new WPDesk_Flexible_Shipping_SaaS_Country_State_Field( null, null );
		}
	}

	/**
	 * Get country state code as single field.
	 *
	 * @return string
	 */
	public function get_as_single_field() {
		$single_field = '';
		if ( ! empty( $this->country_code ) ) {
			$single_field = $this->country_code;
			if ( ! empty( $this->state_code ) ) {
				$single_field .= self::DELIMITER . $this->state_code;
			}
		}
		return $single_field;
	}

	/**
	 * Get country state as array.
	 *
	 * @return array
	 */
	public function get_as_array() {
		$array = [];
		if ( ! empty( $this->country_code ) ) {
			$array[ self::COUNTRY_CODE_FIELD ] = $this->country_code;
			if ( ! empty( $this->state_code ) ) {
				$array[ self::STATE_CODE_FIELD ] = $this->state_code;
			}
		}
		return $array;
	}
}
