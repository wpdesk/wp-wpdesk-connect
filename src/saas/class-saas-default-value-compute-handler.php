<?php

/**
 * Handles rate request.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Default_Value_Compute_Handler {

	/**
	 * Compute method.
	 *
	 * @var string
	 */
	private $compute_method;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Default_Value_Compute_Handler constructor.
	 *
	 * @param string $compute_method Compute method.
	 */
	public function __construct( $compute_method ) {
		$this->compute_method = $compute_method;
	}

	/**
	 * Compute.
	 */
	public function compute() {
		$computed_value = null;
		if ( 'units' === $this->compute_method ) {
			return $this->compute_units();
		}
		return $computed_value;
	}

	/**
	 * Compute units.
	 *
	 * @return string
	 */
	private function compute_units() {
		$units                   = 'imperial';
		$woocommerce_weight_unit = get_option( 'woocommerce_weight_unit', 'lbs' );
		if ( in_array( $woocommerce_weight_unit, array( 'g', 'kg' ), true ) ) {
			$units = 'metric';
		}
		return $units;
	}

}
