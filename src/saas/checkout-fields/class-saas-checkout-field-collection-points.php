<?php

/**
 * Displays collection points in checkout.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Collection_Points_Checkout_Field
 */
class WPDesk_Flexible_Shipping_SaaS_Checkout_Field_Collection_Points {

	const FLEXIBLE_SHIPPING_COLLECTION_POINT = 'flexible_shipping_collection_point';

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Service collection points.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points
	 */
	private $service_collection_points;

	/**
	 * Renderer.
	 *
	 * @var WPDesk\View\Renderer\Renderer;
	 */
	private $renderer;

	/**
	 * Request data.
	 *
	 * @var WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data
	 */
	private $request_data;

	/**
	 * Field.
	 *
	 * @var array
	 */
	private $field;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Collection_Points_Checkout_Field constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service                   $shipping_service Shipping service.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points Service collection points.
	 * @param \WPDesk\View\Renderer\Renderer                                   $renderer Renderer.
	 * @param null|WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data         $request_data Request data.
	 * @param array                                                            $field Field.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points,
		WPDesk\View\Renderer\Renderer $renderer,
		$request_data,
		$field
	) {
		$this->shipping_service          = $shipping_service;
		$this->service_collection_points = $service_collection_points;
		$this->renderer                  = $renderer;
		$this->request_data              = $request_data;
		$this->field                     = $field;
	}


	/**
	 * Get request data.
	 *
	 * @return array
	 */
	private function get_post_data_from_request() {
		if ( ! empty( $_REQUEST['post_data'] ) ) {
			parse_str( $_REQUEST['post_data'], $post_data );
		} else {
			$post_data = array();
		}
		return $post_data;
	}

	/**
	 * Prepare collection points options.
	 *
	 * @return array
	 */
	private function prepare_collection_points_options() {
		$address = new \WPDesk\SaasPlatformClient\Model\Shipment\Address();
		$address->setAddressLine1( $this->request_data->get_shipping_address_1() );
		$address->setAddressLine2( $this->request_data->get_shipping_address_2() );
		$address->setPostalCode( $this->request_data->get_shipping_postcode() );
		$address->setCity( $this->request_data->get_shipping_city() );
		$address->setStateCode( $this->request_data->get_shipping_state() );
		$address->setCountryCode( $this->request_data->get_shipping_country() );

		return $this->service_collection_points->prepare_collection_points_options_for_address( $address );
	}

	/**
	 * Display collection point on checkout.
	 */
	public function display_collection_point_field_on_checkout() {
		$collection_points_options = $this->prepare_collection_points_options();
		$selected_collection_point = '';
		$post_data                 = $this->get_post_data_from_request();
		$field_name                = $this->field['id'] . '_' . $this->shipping_service->get_integration_id();
		$field_label               = $this->field['name'];
		$field_description         = empty( $this->field['description'] ) ? '' : $this->field['description'];
		if ( isset( $post_data[ $field_name ] ) && array_key_exists( $post_data[ $field_name ], $collection_points_options ) ) {
			$selected_collection_point = $post_data[ $field_name ];
		}
		echo $this->renderer->render( // phpcs:ignore
			'checkout/shipping-method-after',
			array(
				'select_options'                      => $collection_points_options, // phpcs:ignore
				'selected_collection_point'           => $selected_collection_point, // phpcs:ignore
				'collection_point_label'              => $field_label, // phpcs:ignore
				'collection_point_description'        => $field_description, // phpcs:ignore
				'collection_point_field_name'         => $field_name, // phpcs:ignore
				'collection_point_map_selector_label' => __( 'Select point from map', 'flexible-shipping' ), // phpcs:ignore
				'collection_point_service_id'         => strval( $this->shipping_service->get_id() ), // phpcs:ignore
			)
		);
	}

	/**
	 * Display collection point for shipment.
	 *
	 * @param string                                 $folder Template folder.
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function display_collection_point_field_for_shipment( $folder, $shipment ) {
		$field_label = $this->field['name'];
		try {
			$collection_point_response = $this->service_collection_points->get_collection_point( $shipment->get_meta( $this->field['id'] ) );
			$field_value               = $this->service_collection_points->format_single_point_description_from_response( $collection_point_response );
		} catch ( Exception $e ) {
			$field_value = $e->getMessage();
		}
		echo $this->renderer->render( // phpcs:ignore
			$folder . '/after_order_table_checkout_field',
			array(
				'field_label' => $field_label, // phpcs:ignore
				'field_value' => $field_value, // phpcs:ignore
			)
		);

	}

}
