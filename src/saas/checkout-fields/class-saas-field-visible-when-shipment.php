<?php

/**
 * Handles conditional logic for shipment.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Shipment_Field_Visible_When
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Shipment extends WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When {

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment
	 */
	private $shipment;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Shipment_Field_Visible_When constructor.
	 *
	 * @param array                             $visible_when_conditions Conditions.
	 * @param WPDesk_Flexible_Shipping_Shipment $shipment Shipment.
	 */
	public function __construct( array $visible_when_conditions, WPDesk_Flexible_Shipping_Shipment $shipment ) {
		parent::__construct( $visible_when_conditions );
		$this->shipment = $shipment;
	}

	/**
	 * Create for field.
	 *
	 * @param array                             $field Field.
	 * @param WPDesk_Flexible_Shipping_Shipment $shipment Shipment.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Shipment
	 */
	public static function create_for_field( array $field, WPDesk_Flexible_Shipping_Shipment $shipment ) {
		$visible_when_conditions = array();
		if ( isset( $field['visible-when'] ) ) {
			$visible_when_conditions = $field['visible-when'];
		}
		return new self( $visible_when_conditions, $shipment );
	}

	/**
	 * Get service type from shipping rate.
	 *
	 * @return string
	 */
	private function get_service_type_from_shipment() {
		$service_type = $this->shipment->get_meta( 'service_type', '' );
		return $service_type;
	}

	/**
	 * Get service type.
	 *
	 * @return string
	 */
	protected function get_service_type() {
		return $this->get_service_type_from_shipment();
	}

}
