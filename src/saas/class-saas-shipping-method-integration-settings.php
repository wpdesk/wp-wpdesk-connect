<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Method_Integration_Settings
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Method_Integration_Settings {

	const TYPE_TEXT            = 'text';
	const TYPE_CHECKBOX        = 'checkbox';
	const TYPE_SELECT          = 'select';
	const TYPE_CUSTOM_SERVICES = 'custom_services';

	const WOOCOMMERCE_SETTINGS_SHIPPING_URL = 'admin.php?page=wc-settings&tab=shipping';

	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Shipping service.
	 *
	 * @var \WPDesk\SaasPlatformClient\Model\ShippingService
	 */
	private $shipping_service;

	/**
	 * Field name prefix.
	 *
	 * @var string
	 */
	private $field_name_prefix;

	/**
	 * Integration ID.
	 *
	 * @var string
	 */
	private $integration_id;

	/**
	 * Supported columns types.
	 *
	 * @var array
	 */
	private $supported_columns_types;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * Live rates handler.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler
	 */
	private $live_rates_handler;

	/**
	 * Shipment auto create.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipment_Auto_Create
	 */
	private $shipment_auto_create;

	/**
	 * Shipment Order auto complete.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipment_Order_Auto_Complete
	 */
	private $shipment_order_auto_complete;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Method_Integration_Settings constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS Connection.
	 * @param \Psr\Log\LoggerInterface                       $logger Logger.
	 */
	public function __construct( $shipping_service, $saas_connection, \Psr\Log\LoggerInterface $logger = null ) {
		$this->shipping_service  = $shipping_service;
		$this->saas_connection   = $saas_connection;
		$this->integration_id    = $shipping_service->get_integration_id();
		$this->field_name_prefix = $this->integration_id . '_';

		$this->supported_columns_types = array( self::TYPE_TEXT, self::TYPE_CHECKBOX, self::TYPE_SELECT, self::TYPE_CUSTOM_SERVICES );

		if ( $shipping_service->supports( 'supportsShipmentLiveRates' ) ) {
			$this->live_rates_handler = new WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler(
				$this->integration_id,
				$shipping_service,
				$saas_connection,
				$logger
			);
		}

		$this->shipment_auto_create = new WPDesk_Flexible_Shipping_SaaS_Shipment_Auto_Create(
			$this->integration_id,
			$shipping_service,
			$logger
		);

		$this->shipment_order_auto_complete = new WPDesk_Flexible_Shipping_SaaS_Shipment_Order_Auto_Complete(
			$this->integration_id,
			$shipping_service,
			$logger
		);

		$this->logger = $logger;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'flexible_shipping_integration_options', array( $this, 'add_shipping_integrations_options' ) );

		add_filter( 'flexible_shipping_method_settings', array( $this, 'get_settings_for_flexible_shipping_method' ), 10, 2 );

		add_action( 'flexible_shipping_method_script', array( $this, 'add_flexible_shipping_integration_script' ) );

		add_filter( 'flexible_shipping_process_admin_options', array( $this, 'process_flexible_shipping_integration_options' ), 10, 1 );

		add_filter( 'flexible_shipping_method_integration_col', array( $this, 'show_integration_in_flexible_shipping_integration_col' ), 10, 2 );

		if ( isset( $this->live_rates_handler ) ) {
			$this->live_rates_handler->hooks();
		}

		$this->shipment_auto_create->hooks();

		$this->shipment_order_auto_complete->hooks();
	}

	/**
	 * Add shipping integrations
	 *
	 * @param array $options Options.
	 *
	 * @return array
	 */
	public function add_shipping_integrations_options( array $options ) {
		$options[ $this->integration_id ] = $this->shipping_service->get_name();
		return $options;
	}

	/**
	 * Add CSS class to fields.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	private function add_css_class_to_settings_fields( array $settings ) {
		foreach ( $settings as $field_key => $field ) {
			$settings[ $field_key ]['class']  = empty( $settings[ $field_key ]['class'] ) ? '' : $settings[ $field_key ]['class'] . ' ';
			$settings[ $field_key ]['class'] .= $this->integration_id;
		}
		return $settings;
	}

	/**
	 * Add CSS class to fields.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	private function remove_required_attribute_from_settings_fields( array $settings ) {
		foreach ( $settings as $field_key => $field ) {
			$custom_attributes_key = 'custom_attributes';
			$custom_attributes     = isset( $field[ $custom_attributes_key ] ) ? $field[ $custom_attributes_key ] : array();
			if ( isset( $custom_attributes['required'] ) ) {
				unset( $custom_attributes['required'] );
			}
			$settings[ $field_key ][ $custom_attributes_key ] = $custom_attributes;
		}
		return $settings;
	}

	/**
	 * Setup shipping integrations settings values.
	 *
	 * @param array $settings Settings fields.
	 * @param array $shipping_method Shipping method.
	 *
	 * @return array
	 */
	private function setup_settings_values( array $settings, array $shipping_method ) {
		foreach ( $settings as $field_key => $field ) {
			if ( isset( $shipping_method[ $field_key ] ) ) {
				$settings[ $field_key ]['default'] = $shipping_method[ $field_key ];
			}
		}
		return $settings;
	}

	/**
	 * Setup shipping integrations custom services values.
	 *
	 * @param array $settings Settings fields.
	 * @param array $shipping_method Shipping method.
	 *
	 * @return array
	 */
	private function setup_custom_services( array $settings, array $shipping_method ) {
		foreach ( $settings as $field_key => $field ) {
			if ( self::TYPE_CUSTOM_SERVICES === $settings[ $field_key ]['type'] ) {
				$custom_services_settings           = new WPDesk_Flexible_Shipping_SaaS_Custom_Services_Settings(
					$settings[ $field_key ],
					isset( $shipping_method[ $field_key ] ) ? $shipping_method[ $field_key ] : array()
				);
				$settings[ $field_key ]['services'] = $custom_services_settings->setup_custom_services();
			}
		}
		return $settings;
	}

	/**
	 * Get settings for shipping method instance.
	 *
	 * @param int $shiping_method_instance_id Shipping method instance ID.
	 *
	 * @return array
	 */
	private function get_settings_for_shipping_method_instance( $shiping_method_instance_id ) {
		$shipping_zone = WC_Shipping_Zones::get_zone_by( 'instance_id', $shiping_method_instance_id );
		$shipping_zone_data_extractor = new WPDesk_Flexible_Shipping_Shipping_Zone_Data_Extractor( $shipping_zone );
		$countries = $shipping_zone_data_extractor->get_countries();
		$settings = $this->shipping_service->prepare_flexible_shipping_integration_settings_fields( $this->field_name_prefix, $countries );
		return $settings;
	}


	/**
	 * Get settings for flexible shipping method.
	 *
	 * @param array $flexible_shipping_settings Flexible Shipping settings.
	 * @param array $shipping_method Shipping method.
	 *
	 * @return array
	 */
	public function get_settings_for_flexible_shipping_method( array $flexible_shipping_settings, array $shipping_method ) {
		try {
			$settings = $this->get_settings_for_shipping_method_instance( intval( $shipping_method['woocommerce_method_instance_id'] ) );
			$settings = $this->remove_required_attribute_from_settings_fields( $settings );
			$settings = $this->setup_settings_values( $settings, $shipping_method );
			$settings = $this->setup_custom_services( $settings, $shipping_method );
		} catch ( WPDesk_Flexible_Shipping_SaaS_Service_Settings_Not_Found $e ) {
			$settings = array(
				$this->field_name_prefix . '_error' => array(
					'type'        => 'saas_connection_error',
					'description' => $this->shipping_service->prepare_settings_not_found_message(),
					'class'       => 'flexible_shipping_saas_connection_error',
				),
			);
		} catch ( Exception $e ) {
			$settings = array(
				$this->field_name_prefix . '_error' => array(
					'type'        => 'saas_connection_error',
					'description' => $this->saas_connection->prepare_fatal_error_notice(),
					'class'       => 'flexible_shipping_saas_connection_error',
				),
			);
		}
		$settings = $this->add_css_class_to_settings_fields( $settings );
		return array_merge( $flexible_shipping_settings, $settings );
	}

	/**
	 * Add Flexible Shipping integration script.
	 */
	public function add_flexible_shipping_integration_script() {
		?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#woocommerce_flexible_shipping_method_integration').change(function() {
					flexible_shipping_saas_integration_options('<?php echo $this->integration_id; ?>');
				});
				flexible_shipping_saas_integration_options('<?php echo $this->integration_id; ?>');
			});
		</script>
		<?php
	}

	/**
	 * Grab Flexible Shipping integration option value.
	 *
	 * @param string $field_key Field key.
	 * @param array  $field Field.
	 *
	 * @return array|string
	 */
	private function grab_flexible_shipping_integration_option_value( $field_key, $field ) {
		$post_field_name = 'woocommerce_flexible_shipping_' . $field_key;
		$field_value     = '';
		$field_type      = $field['type'];
		if ( self::TYPE_CHECKBOX === $field_type ) {
			$field_value = '0';
		}
		if ( isset( $_POST[ $post_field_name ] ) ) {
			$field_post_value = $_POST[ $post_field_name ];
			if ( in_array( $field_type, $this->supported_columns_types, true ) ) {
				if ( self::TYPE_CUSTOM_SERVICES === $field_type ) {
					$custom_services_settings = new WPDesk_Flexible_Shipping_SaaS_Custom_Services_Settings(
						$field_post_value,
						array()
					);

					$field_value = $custom_services_settings->process_custom_services_field( $field_post_value );
				} else {
					$field_value = sanitize_text_field( $field_post_value );
					if ( self::TYPE_CHECKBOX === $field_type ) {
						if ( '1' === $field_value ) {
							$field_value = 'yes';
						}
					}
				}
			}
		}
		return $field_value;
	}

	/**
	 * Process Flexible Shipping integration options.
	 *
	 * @param array $shipping_method_options Shipping method options.
	 *
	 * @return mixed
	 */
	public function process_flexible_shipping_integration_options( array $shipping_method_options ) {
		if ( isset( $shipping_method_options['method_integration'] )
			&& $this->integration_id === $shipping_method_options['method_integration']
		) {
			try {
				$settings = $this->get_settings_for_shipping_method_instance( intval( $shipping_method_options['woocommerce_method_instance_id'] ) );
				foreach ( $settings as $field_key => $field ) {
					$shipping_method_options[ $field_key ] = $this->grab_flexible_shipping_integration_option_value( $field_key, $field );
				}
			} catch ( Exception $e ) {
				$this->saas_connection->add_notice_fatal_error();
				do_action( 'admin_notices' );
			}
		}
		return $shipping_method_options;
	}

	/**
	 * Show integration name in Flexible Shipping integration column.
	 *
	 * @param string $col Col value.
	 * @param array  $shipping_method Shipping method.
	 *
	 * @return string
	 */
	public function show_integration_in_flexible_shipping_integration_col( $col, array $shipping_method ) {
		if ( isset( $shipping_method['method_integration'] ) && $this->integration_id === $shipping_method['method_integration'] ) {
			$col = sprintf( '<td width="1%%" class="integration default"><span>%1$s</span></td>', $this->shipping_service->get_name() );
		}
		return $col;
	}


}
