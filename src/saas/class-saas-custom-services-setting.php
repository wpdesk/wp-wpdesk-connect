<?php

/**
 * Custom Services - settings field.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Custom_Services_Settings
 */
class WPDesk_Flexible_Shipping_SaaS_Custom_Services_Settings {

	const CUSTOM_SERVICE_NAME    = 'name';
	const CUSTOM_SERVICE_ENABLED = 'enabled';

	/**
	 * Field.
	 *
	 * @var array
	 */
	private $field;

	/**
	 * Shipping method field.
	 *
	 * @var array
	 */
	private $shipping_method_field;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Custom_Services_Settings constructor.
	 *
	 * @param array      $field Field.
	 * @param array|null $shipping_method_field Shipping method field.
	 */
	public function __construct( array $field, $shipping_method_field ) {
		$this->field                 = $field;
		$this->shipping_method_field = null === $shipping_method_field ? array() : $shipping_method_field;
		if ( ! is_array( $this->shipping_method_field ) ) {
			$this->shipping_method_field = array();
		}
	}

	/**
	 * Shipping method first time setup?
	 *
	 * @return bool
	 */
	private function first_time_setup() {
		if ( 0 === count( $this->shipping_method_field ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Create services field.
	 *
	 * @return array
	 */
	private function create_services_field() {
		$services = array();
		if ( isset( $this->field['services'] ) && is_array( $this->field['services'] ) ) {
			foreach ( $this->field['services'] as $service_id => $service ) {
				if ( isset( $this->shipping_method_field[ $service_id ] ) ) {
					$shipping_method_field_service = $this->shipping_method_field[ $service_id ];

					$services[ $service_id ][ self::CUSTOM_SERVICE_NAME ] = $shipping_method_field_service[ self::CUSTOM_SERVICE_NAME ];
					if ( isset( $shipping_method_field_service[ self::CUSTOM_SERVICE_ENABLED ] ) ) {
						$services[ $service_id ][ self::CUSTOM_SERVICE_ENABLED ] = $shipping_method_field_service[ self::CUSTOM_SERVICE_ENABLED ];
					} else {
						$services[ $service_id ][ self::CUSTOM_SERVICE_ENABLED ] = '0';
					}
				} else {
					$services[ $service_id ] = array( self::CUSTOM_SERVICE_NAME => $service[ self::CUSTOM_SERVICE_NAME ] );
				}
			}
		}
		return $services;
	}

	/**
	 * Remove unused services from shipping method field.
	 *
	 * @param array $services Services.
	 */
	private function remove_unused_services( $services ) {
		foreach ( $this->shipping_method_field as $service_id => $service ) {
			if ( empty( $services[ $service_id ] ) ) {
				unset( $this->shipping_method_field[ $service_id ] );
			}
		}
	}

	/**
	 * Set default values when service fields are empty.
	 *
	 * @param array  $services Services.
	 * @param string $default_enabled Default value for enabled field.
	 *
	 * @return array
	 */
	private function set_empty_service_fields( $services, $default_enabled ) {
		foreach ( $services as $service_id => $service ) {
			if ( ! isset( $services[ $service_id ][ self::CUSTOM_SERVICE_ENABLED ] ) ) {
				$services[ $service_id ][ self::CUSTOM_SERVICE_ENABLED ] = $default_enabled;
			}
			if ( empty( $services[ $service_id ][ self::CUSTOM_SERVICE_NAME ] ) ) {
				$services[ $service_id ][ self::CUSTOM_SERVICE_NAME ] = $this->field['services'][ $service_id ][ self::CUSTOM_SERVICE_NAME ];
			}
		}
		return $services;
	}

	/**
	 * Sort services as in settings.
	 *
	 * @param array $services Services.
	 *
	 * @return array
	 */
	private function sort_services( array $services ) {
		if ( 0 !== count( $this->shipping_method_field ) ) {
			$services = array_replace( array_flip( array_keys( $this->shipping_method_field ) ), $services );
		}
		return $services;
	}

	/**
	 * Setup shipping integrations custom services values.
	 *
	 * @return array
	 */
	public function setup_custom_services() {
		if ( $this->first_time_setup() ) {
			$default_enabled = '1';
		} else {
			$default_enabled = '0';
		}
		$services = $this->create_services_field();

		$this->remove_unused_services( $services );

		$services = $this->sort_services( $services );

		$services = $this->set_empty_service_fields( $services, $default_enabled );

		return $services;
	}


	/**
	 * Process custom services field.
	 *
	 * @param array $posted_value Posted value.
	 *
	 * @return array
	 */
	public function process_custom_services_field( array $posted_value ) {
		$custom_services_settings_value = array();
		foreach ( $posted_value as $service_id => $service ) {
			$custom_services_settings_value[ sanitize_text_field( $service_id ) ] = array(
				self::CUSTOM_SERVICE_NAME    => sanitize_text_field( $posted_value[ $service_id ][ self::CUSTOM_SERVICE_NAME ] ),
				self::CUSTOM_SERVICE_ENABLED => 0,
			);
			if ( ! empty( $posted_value[ $service_id ][ self::CUSTOM_SERVICE_ENABLED ] ) ) {
				$custom_services_settings_value[ sanitize_text_field( $service_id ) ][ self::CUSTOM_SERVICE_ENABLED ] = 1;
			}
		}
		return $custom_services_settings_value;
	}

}
