<?php

/**
 * Class WPDesk_Flexible_Shipping_Money_Formatter
 */
class WPDesk_Flexible_Shipping_Money_Formatter {

	/**
	 * Money.
	 *
	 * @var \Money\Money
	 */
	private $money;

	/**
	 * WPDesk_Flexible_Shipping_Money_Formatter constructor.
	 *
	 * @param \Money\Money $money Money.
	 */
	public function __construct( Money\Money $money ) {
		$this->money = $money;
	}

	/**
	 * Format as money string.
	 *
	 * @return null|string
	 */
	public function format() {
		if ( ! empty( $this->money ) ) {
			if ( class_exists( 'NumberFormatter' ) ) {
				$currencies       = new \Money\Currencies\ISOCurrencies();
				$number_formatter = new NumberFormatter( get_locale(), NumberFormatter::CURRENCY );
				$money_formatter  = new \Money\Formatter\IntlMoneyFormatter( $number_formatter, $currencies );
				return $money_formatter->format( $this->money );
			} else {
				return wc_price( $this->money->getAmount() / 100, array( 'currency' => trim( $this->money->getCurrency() ) ) );
			}
		}
		return null;

	}

}
