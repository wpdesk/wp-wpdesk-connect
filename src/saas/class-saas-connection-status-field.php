<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings_Connection
 */
class WPDesk_Flexible_Shipping_SaaS_Connection_Status_Field {

	/**
	 * Shipping method.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings
	 */
	private $shipping_method;

	/**
	 * WPDesk_Flexible_Shipping_Connection_Status_Field constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings $shipping_method Shipping method.
	 */
	public function __construct( $shipping_method ) {
		$this->shipping_method = $shipping_method;
	}

	/**
	 * Get field key.
	 *
	 * @param string $key Key.
	 *
	 * @return string
	 */
	protected function get_field_key( $key ) {
		return $this->shipping_method->get_field_key( $key );
	}

	/**
	 * Generate HTML.
	 *
	 * @param string $key Key.
	 * @param array  $data Data.
	 *
	 * @return false|string
	 */
	public function generate_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		$field_id           = esc_attr( $field_key );
		$tooltip_html       = $this->shipping_method->get_tooltip_html( $data );
		$title              = $data['title'];
		$description_html   = $this->shipping_method->get_description_html( $data );
		$status_html        = __( 'Checking...', 'flexible-shipping' );
		$service_id         = $this->shipping_method->get_shipping_service_id();
		$ajax_action        = false;
		$ajax_nonce         = '';
		$ajax_error_message = __( 'Something go wrong! (AJAX)', 'flexible-shipping' );

		if ( ! $this->shipping_method->get_has_saas_settings() ) {
			$status_html = __( 'Add credentials', 'flexible-shipping' );
		} else {
			$ajax_action = WPDesk_Flexible_Shipping_SaaS_Connection_Status_Ajax::AJAX_ACTION;
			$ajax_nonce  = wp_create_nonce( WPDesk_Flexible_Shipping_SaaS_Connection_Status_Ajax::NONCE_NAME );
		}

		ob_start();
		include 'views/html-field-connection-status.php';
		return ob_get_clean();
	}

}
