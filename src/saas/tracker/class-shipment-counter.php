<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Counter
 */
class WPDesk_Flexible_Shipping_Shipment_Counter {

	const COUNTER_SHIPMENTS = 'shipments';
	const FIRST_SHIPMENT    = 'first_shipment';
	const LAST_SHIPMENT     = 'last_shipment';
	const ALL_SHIPMENTS     = 'all_shipments';
	const STATUS            = 'status';


	/**
	 * Integration.
	 *
	 * @var string
	 */
	private $integration;

	/**
	 * Wpdb.
	 *
	 * @var wpdb
	 */
	private $wpdb;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Counter constructor.
	 *
	 * @param string $integration Integration.
	 * @param wpdb   $wpdb wpdb.
	 */
	public function __construct( $integration, $wpdb ) {
		$this->integration = $integration;
		$this->wpdb        = $wpdb;
	}

	/**
	 * Set counters from row.
	 *
	 * @param array  $counters Counters.
	 * @param object $row Row.
	 *
	 * @return array
	 */
	private function set_counters( array $counters, $row ) {
		$counters[ self::STATUS ][ $row->shipment_status ] = array(
			self::COUNTER_SHIPMENTS => $row->shipments,
			self::FIRST_SHIPMENT    => $row->first_shipment,
			self::LAST_SHIPMENT     => $row->last_shipment,
		);

		$counters[ self::ALL_SHIPMENTS ]  = $counters[ self::ALL_SHIPMENTS ] + $row->shipments;
		$counters[ self::FIRST_SHIPMENT ] = ! isset( $counters[ self::FIRST_SHIPMENT ] ) ? $row->first_shipment : min( $counters[ self::FIRST_SHIPMENT ], $row->first_shipment );
		$counters[ self::LAST_SHIPMENT ]  = ! isset( $counters[ self::LAST_SHIPMENT ] ) ? $row->last_shipment : max( $counters[ self::LAST_SHIPMENT ], $row->last_shipment );

		return $counters;
	}

	/**
	 * Count shipments.
	 *
	 * @return array
	 */
	public function count_shipments() {
		$counters = array( self::ALL_SHIPMENTS => 0 );
		$query    = $this->wpdb->get_results(
			$this->wpdb->prepare(
				"SELECT count(p.ID) AS shipments, p.post_status AS shipment_status, min(p.post_date) AS first_shipment, max(p.post_date) AS last_shipment
				FROM {$this->wpdb->posts} p, {$this->wpdb->postmeta} m
				WHERE p.post_type = %s
					AND p.ID = m.post_id
					AND m.meta_key = %s
					AND m.meta_value = %s
				GROUP BY p.post_status",
				WPDesk_Flexible_Shipping_Shipment_CPT::POST_TYPE_SHIPMENT,
				'_integration',
				$this->integration
			)
		);
		if ( $query ) {
			foreach ( $query as $row ) {
				$counters = $this->set_counters( $counters, $row );
			}
		}
		return $counters;
	}

}
