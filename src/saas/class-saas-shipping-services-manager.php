<?php
/**
 * Class WPDesk_Flexible_Shipping_Shipping_Methods_Manager
 */

class WPDesk_Flexible_Shipping_SaaS_Services_Manager
	implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant, \WPDesk\PluginBuilder\Plugin\HookableCollection
{

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;
	use \WPDesk\PluginBuilder\Plugin\HookableParent;

	const OPTION_ALL_SHIPPING_SERVICES     = 'fs_saas_all_shipping_servicess';
	const OPTION_ENABLED_SHIPPING_SERVICES = 'fs_saas_enabled_shipping_services';

	const PARAMETER_SETTINGS_UPDATED       = 'settings-updated';
	const PARAMETER_SETTINGS_UPDATED_VALUE = 'flexible-shipping';

	const LOGGER_SOURCE = 'saas-methods-manager';

	const PRIORITY_FIRST = 1;


	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Flexible Shipping Connect shipping method.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Settings
	 */
	private $shipping_method_connect = null;

	/**
	 * SaaS shipping methods.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings[]
	 */
	private $saas_shipping_methods = array();

	/**
	 * SaaS shipping services.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service[]
	 */
	private $saas_shipping_services = array();

	/**
	 * Require UK states - there is enabled shipping method with requireUKStates capability.
	 *
	 * @var bool
	 */
	private $require_uk_states = false;

	/**
	 * Links.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
	 */
	private $saas_platform_links;

	/**
	 * Renderer.
	 *
	 * @var WPDesk\View\Renderer\Renderer;
	 */
	private $renderer;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * Settings integration checkbox.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox
	 */
	private $integration_checkbox;

	/**
	 * WPDesk_Flexible_Shipping_Services constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection     $saas_connection Connection.
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Links.
	 * @param WPDesk\View\Renderer\Renderer                $renderer Renderer.
	 * @param \Psr\Log\LoggerInterface                     $logger Logger.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection,
		WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links,
		WPDesk\View\Renderer\Renderer $renderer,
		WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox $integration_checkbox,
		\Psr\Log\LoggerInterface $logger = null
	) {
		$this->saas_connection = $saas_connection;
		$this->renderer        = $renderer;
		if ( null !== $logger ) {
			$this->logger = $logger;
		} else {
			$this->logger = WPDesk_Flexible_Shipping_Logger_Factory::create_logger();
		}

		$this->saas_platform_links  = $saas_platform_links;
		$this->integration_checkbox = $integration_checkbox;
		$this->create_saas_shipping_methods();

	}

	/**
	 * Hooks.
	 */
	public function hooks() {

		/** Important! Must be added before regular FS shipping method because of COD `Enable for shipping methods` functionality. */
		add_filter( 'woocommerce_shipping_methods',
			array( $this, 'add_flexible_shipping_connect_shipping_method' ), self::PRIORITY_FIRST
		);
		add_filter( 'woocommerce_shipping_methods', array( $this, 'add_shipping_methods_for_shipping_services' ) );

		add_filter( 'woocommerce_shipping_zone_shipping_methods',
			array( $this, 'inject_saas_connection_to_shipping_methods' )
		);

		add_action( 'woocommerce_shipping_zone_method_added', array( $this, 'set_shipping_method_type' ), 10, 3 );
		add_action( 'woocommerce_shipping_zone_method_deleted', array( $this, 'delete_shipping_method_type' ), 10,
			3
		);

		add_action( 'current_screen', array( $this, 'clear_cache_at_settings_page' ) );
		add_action( 'current_screen', array( $this, 'handle_settings_updated_notice' ) );

		add_filter( 'flexible_shipping_shipment_class', array( $this, 'flexible_shipping_shipment_class' ), 10, 2 );

		$this->hooks_on_hookable_objects();

	}

	/**
	 * Flexible Shipping shipment class for SaaS integration
	 *
	 * @param string $class_name Class name.
	 * @param string $integration Integration.
	 *
	 * @return string
	 */
	public function flexible_shipping_shipment_class( $class_name, $integration ) {
		if ( isset( $this->saas_shipping_methods[ $integration ] ) ) {
			return WPDesk_Flexible_Shipping_Shipment_Saas::class;
		}

		return $class_name;
	}

	/**
	 * Handle settings updated notice.
	 *
	 * @param WP_Screen $current_screen Current screen.
	 */
	public function handle_settings_updated_notice( $current_screen ) {
		if ( $this->is_in_shipping_settings( $current_screen ) ) {
			if ( isset( $_GET[ self::PARAMETER_SETTINGS_UPDATED ] )
			     && $_GET[ self::PARAMETER_SETTINGS_UPDATED ] == self::PARAMETER_SETTINGS_UPDATED_VALUE
			) {
				new \WPDesk\Notice\Notice(
					// Translators: strong tag.
					sprintf( __( '%1$sYour settings have been saved.%2$s', 'flexible-shipping' ), '<strong>',
						'</strong>'
					),
					'updated'
				);
			}
		}
	}

	/**
	 * Create shipping method.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings
	 */
	private function create_shipping_method( $shipping_service ) {
		$shipping_method = new WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings( 0, $shipping_service );
		$shipping_method->set_saas_connection( $this->saas_connection );
		$shipping_method->set_shipping_methods_manager( $this );
		$shipping_service->set_shipping_method( $shipping_method );

		return $shipping_method;
	}

	/**
	 * Inject SaaS connection to shipping methods.
	 *
	 * @param array $shipping_methods Shipping methods.
	 *
	 * @return mixed
	 */
	public function inject_saas_connection_to_shipping_methods( array $shipping_methods ) {
		foreach ( $shipping_methods as $shipping_method_key => $shipping_method ) {
			if ( $shipping_method instanceof WPDesk_Flexible_Shipping_Saas_Connection_Aware ) {
				$shipping_method->set_saas_connection( $this->saas_connection );
				$shipping_method->set_shipping_methods_manager( $this );
			}
		}

		return $shipping_methods;
	}

	/**
	 * Create shipping method integration settings.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Method_Integration_Settings
	 */
	private function create_shipping_method_integration_settings( $shipping_service ) {
		$integration_settings = new WPDesk_Flexible_Shipping_SaaS_Shipping_Method_Integration_Settings( $shipping_service, $this->saas_connection, $this->logger );
		$integration_settings->hooks();

		return $integration_settings;
	}

	/**
	 * Create shipping method integration settings.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Add_Shipping_Handler
	 */
	private function create_add_shipping_options_handler( $shipping_service ) {
		return new WPDesk_Flexible_Shipping_SaaS_Add_Shipping_Handler( $shipping_service );
	}

	/**
	 * Create shipping service.
	 *
	 * @param array $shipping_service_data Shipping service data.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private function create_shipping_service( array $shipping_service_data ) {
		if ( empty( $shipping_service_data['settings_fields'] ) ) {
			$shipping_service_data['settings_fields'] = array();
		}
		if ( empty( $shipping_service_data['settings_fields_values'] ) ) {
			$shipping_service_data['settings_fields_values'] = array();
		}
		if ( empty( $shipping_service_data['request_fields'] ) ) {
			$shipping_service_data['request_fields'] = array();
		}
		if ( empty( $shipping_service_data['capabilities'] ) ) {
			$shipping_service_data['capabilities'] = array();
		}
		if ( empty( $shipping_service_data['promoted'] ) ) {
			$shipping_service_data['promoted'] = false;
		}

		$shipping_service = new WPDesk_Flexible_Shipping_SaaS_Shipping_Service(
			$this->saas_connection,
			$this->saas_platform_links,
			$shipping_service_data['id'],
			$shipping_service_data['name'],
			$shipping_service_data['description'],
			$shipping_service_data['promoted'],
			$shipping_service_data['settings_fields'],
			$shipping_service_data['settings_fields_values'],
			$shipping_service_data['request_fields'],
			$shipping_service_data['capabilities']
		);

		return $shipping_service;
	}

	/**
	 * Create shipping methods.
	 */
	public function create_saas_shipping_methods() {
		if ( $this->saas_connection->is_connected() ) {

			$all_services     = $this->get_all_services();
			$enabled_services = $this->get_enabled_services();
			try {
				foreach ( $all_services as $service_id => $shipping_service_data ) {
					$shipping_service = $this->create_shipping_service( $shipping_service_data );

					$this->saas_shipping_services[ $service_id ] = $shipping_service;
					if ( in_array( $service_id, $enabled_services, true ) ) {
						$shipping_method = $this->create_shipping_method( $shipping_service );

						$this->saas_shipping_methods[ $shipping_method->id ] = $shipping_method;

						$this->create_shipping_method_integration_settings( $shipping_service );

						$shipping_options_handler = $this->create_add_shipping_options_handler( $shipping_service );
						$shipping_options_handler->hooks();

						if ( $shipping_service->supports( 'requireUKStates' ) ) {
							$this->require_uk_states = true;
						}

						$collection_points = new WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points(
							$this->saas_connection,
							$shipping_service
						);
						$this->add_hookable( $collection_points );
						$shipping_service->set_collection_points( $collection_points );

						$checkout_fields = new WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Checkout(
							$shipping_service,
							$collection_points,
							$this->renderer
						);
						$this->add_hookable( $checkout_fields );

					}
				}
			} catch ( Exception $e ) {
				$this->logger->debug( "create_saas_shipping_methods method exception: {$e->getMessage()} code: {$e->getCode()}",
					[
						'exception' => $e,
					]
				);
			}
		}
	}

	/**
	 * In settings screen?
	 *
	 * @param WP_Screen $current_screen Current screen.
	 *
	 * @return bool
	 */
	private function is_in_shipping_settings( $current_screen = null ) {
		if ( ! isset( $current_screen ) ) {
			if ( function_exists( 'get_current_screen' ) ) {
				$current_screen = get_current_screen();
			}
		}
		if ( isset( $current_screen ) ) {
			$tab_parameter = 'tab';
			if ( 'woocommerce_page_wc-settings' === $current_screen->id
			     && isset( $_GET[ $tab_parameter ] )
			     && 'shipping' === $_GET[ $tab_parameter ]
			) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Clear SaaS Cache on shipping settings pages.
	 *
	 * @param WP_Screen $current_screen Current screen.
	 */
	public function clear_cache_at_settings_page( $current_screen ) {
		if ( $this->is_in_shipping_settings( $current_screen ) ) {
			$this->saas_connection->clear_saas_client_cache();
		}
	}

	/**
	 * Set shipping method type - fired on woocommerce_shipping_zone_method_added action.
	 *
	 * @param int $instance_id Instance ID.
	 * @param string $type Type.
	 * @param int $zone_id Zone ID.
	 */
	public function set_shipping_method_type( $instance_id, $type, $zone_id ) {
		update_option( WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings::OPTION_SHIPPING_METHOD_TYPE . $instance_id,
			$type
		);
	}

	/**
	 * Delete shipping method type - fired on woocommerce_shipping_zone_method_deleted action.
	 *
	 * @param int $instance_id Instance ID.
	 * @param int $method_id Method ID.
	 * @param int $zone_id Zone ID.
	 */
	public function delete_shipping_method_type( $instance_id, $method_id, $zone_id ) {
		delete_option( WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings::OPTION_SHIPPING_METHOD_TYPE . $instance_id );
	}

	/**
	 * Add flexible shipping connect shipping method to woocommerce shipping methods.
	 *
	 * @param array $methods Methods.
	 *
	 * @return array
	 */
	public function add_flexible_shipping_connect_shipping_method( $methods ) {
		if ( null === $this->shipping_method_connect ) {
			$this->shipping_method_connect = new WPDesk_Flexible_Shipping_SaaS_Settings(
				0,
				$this->saas_connection,
				$this->saas_platform_links,
				null,
				$this->integration_checkbox
			);
			$this->shipping_method_connect->set_shipping_methods_manager( $this );
		}
		$methods[ WPDesk_Flexible_Shipping_SaaS_Settings::METHOD_ID ] = $this->shipping_method_connect;

		return $methods;
	}

	/**
	 * Add shipping methods for shipping services.
	 *
	 * @param array $methods Methods.
	 *
	 * @return mixed
	 */
	public function add_shipping_methods_for_shipping_services( $methods ) {
		foreach ( $this->saas_shipping_methods as $method_id => $shipping_method ) {
			$methods[ $method_id ] = $shipping_method;
		}

		return $methods;
	}

	/**
	 * Update all shipping services.
	 *
	 * @param array $shipping_services All shipping services.
	 */
	public function update_all_shipping_services( array $shipping_services ) {
		$all_shipping_services = array();
		/**
		 * IDE type hint.
		 *
		 * @var \WPDesk\SaasPlatformClient\Model\ShippingService $service
		 */
		foreach ( $shipping_services as $service ) {
			$all_shipping_services[ $service->getId() ] = array(
				'id'                     => $service->getId(),
				'name'                   => $service->getName(),
				'description'            => $service->getDescription(),
				'promoted'               => $service->getPromoted(),
				'logo_url'               => $service->getLogoUrl(),
				'settings_fields'        => $service->getConnectionSettingsDefinitionJson(),
				'settings_fields_values' => $service->getConnectionSettingsValuesJsonSchema(),
				'request_fields'         => $service->getRequestFieldsDefinitionJson(),
				'capabilities'           => $service->getCapabilitiesJson(),
			);
		}
		update_option( self::OPTION_ALL_SHIPPING_SERVICES, $all_shipping_services );
	}

	/**
	 * Update enabled shipping services.
	 *
	 * @param array $enabled_shipping_services Enabled shipping services.
	 */
	public function update_enabled_shipping_services( array $enabled_shipping_services = array() ) {
		update_option( self::OPTION_ENABLED_SHIPPING_SERVICES, $enabled_shipping_services );
	}

	/**
	 * Get require uk states.
	 *
	 * @return bool
	 */
	public function is_require_uk_states() {
		return $this->require_uk_states;
	}

	/**
	 * Get all services.
	 *
	 * @return array
	 */
	public function get_all_services() {
		return get_option( self::OPTION_ALL_SHIPPING_SERVICES, array() );
	}

	/**
	 * Get enabled services.
	 *
	 * @return array
	 */
	public function get_enabled_services() {
		return get_option( self::OPTION_ENABLED_SHIPPING_SERVICES, array() );
	}

	/**
	 * Get SaaS shipping service.
	 *
	 * @param int $service_id Service Id.
	 *
	 * @return null|WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	public function get_saas_shiping_service( $service_id ) {
		return isset( $this->saas_shipping_services[ $service_id ] ) ? $this->saas_shipping_services[ $service_id ] : null;
	}

	/**
	 * Is enabled SaaS service.
	 *
	 * @param int $service_id Service Id.
	 *
	 * @return bool
	 */
	public function is_enabled_saas_service( $service_id ) {
		$enabled_services = $this->get_enabled_services();
		return isset( $enabled_services[ $service_id ] );
	}

}
