<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Maintenance_Mode
 */
class WPDesk_Flexible_Shipping_SaaS_Maintenance_Mode_Exception extends RuntimeException {

	/**
	 * Response
	 *
	 * @var \WPDesk\SaasPlatformClient\Response\Response
	 */
	private $response;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Maintenance_Mode_Exception constructor.
	 *
	 * @param \WPDesk\SaasPlatformClient\Response\Response $response Response.
	 * @param string                                       $message Message.
	 * @param int                                          $code Code.
	 * @param Throwable|null                               $previous Previous.
	 */
	public function __construct( WPDesk\SaasPlatformClient\Response\Response $response, $message = '', $code = 0, Throwable $previous = null ) {
		parent::__construct( $message, $code, $previous );
		$this->response = $response;
	}

	/**
	 * Get response.
	 *
	 * @return \WPDesk\SaasPlatformClient\Response\Response
	 */
	public function get_response() {
		return $this->response;
	}

}
