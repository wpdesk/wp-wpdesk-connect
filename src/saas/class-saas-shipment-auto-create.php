<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Shipment_Auto_Create
 */
class WPDesk_Flexible_Shipping_SaaS_Shipment_Auto_Create {

	/**
	 * Integration id.
	 *
	 * @var string
	 */
	private $integration_id;

	/**
	 * Shipping service.
	 *
	 * @var \WPDesk\SaasPlatformClient\Model\ShippingService
	 */
	private $shipping_service;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipment_Settings_Handler constructor.
	 *
	 * @param string                                         $integration_id Integration id.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 * @param \Psr\Log\LoggerInterface                       $logger Logger.
	 */
	public function __construct( $integration_id, $shipping_service, $logger ) {
		$this->integration_id   = $integration_id;
		$this->shipping_service = $shipping_service;
		$this->logger           = $logger;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'woocommerce_order_status_changed', array( $this, 'handle_shipment_create' ), 10, 3 );
	}

	/**
	 * Handle shipment create.
	 *
	 * @param int    $order_id Order ID.
	 * @param string $old_status Old status.
	 * @param string $new_status New status.
	 */
	public function handle_shipment_create( $order_id, $old_status, $new_status ) {
		if ( $this->shipping_service->is_auto_shipment_settings_auto_create() ) {
			if ( $this->shipping_service->is_order_status_shipment_settings_order_status( $new_status ) ) {
				$shipments = fs_get_order_shipments( $order_id, $this->integration_id );
				foreach ( $shipments as $shipment ) {
					if ( $shipment->is_status_fs_new() ) {
						try {
							$shipment->set_sent_via_auto();
							$shipment->api_create();
						} catch ( Exception $e ) {
							$this->logger->error( "Auto creating shipment error {$e->getMessage()}; {$e->getCode()}" );
						}
					}
				}
			}
		}
	}

}
