<?php

/**
 * Interface WPDesk_Flexible_Shipping_Saas_Connection_Aware
 */
interface WPDesk_Flexible_Shipping_Saas_Connection_Aware {

	/**
	 * Set SaaS Connection.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection SaaS connection.
	 */
	public function set_saas_connection( $saas_connection );

	/**
	 * Set shipping methods manager.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Services_Manager $shipping_methods_manager Shipping methods manager.
	 */
	public function set_shipping_methods_manager( $shipping_methods_manager );

}
