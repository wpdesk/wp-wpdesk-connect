<?php

/**
 * Handles woocommerce AJAX request data.
 *
 * Class WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data
 */
class WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data {

	/**
	 * POST data.
	 *
	 * @var array
	 */
	protected $post_data = array();

	/**
	 * WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data constructor.
	 *
	 * @param array $request Request array.
	 */
	public function __construct( array $request ) {
		if ( ! empty( $request['post_data'] ) ) {
			parse_str( $request['post_data'], $this->post_data );
		}
	}

	/**
	 * Get data by name;
	 *
	 * @param string $name Name.
	 *
	 * @return string|null
	 */
	public function get_data_by_name( $name ) {
		return isset( $this->post_data[ $name ] ) ? $this->post_data[ $name ] : null;
	}

	/**
	 * Ship to difrrent address?
	 *
	 * @return bool
	 */
	private function ship_to_different_address() {
		$ship_to_different_address = $this->get_data_by_name( 'ship_to_different_address' );
		return isset( $ship_to_different_address ) && '1' === $ship_to_different_address;
	}

	/**
	 * Get shipping field value.
	 *
	 * @param string $field_name Field name.
	 *
	 * @return string
	 */
	private function get_shipping_field_value( $field_name ) {
		if ( $this->ship_to_different_address() ) {
			$field_value = $this->get_data_by_name( 'shipping_' . $field_name );
		} else {
			$field_value = $this->get_data_by_name( 'billing_' . $field_name );
		}
		return is_null( $field_value ) ? '' : $field_value;
	}

	/**
	 * Get shipping address_1.
	 *
	 * @return string
	 */
	public function get_shipping_address_1() {
		return $this->get_shipping_field_value( 'address_1' );
	}

	/**
	 * Get shipping address_2.
	 *
	 * @return string
	 */
	public function get_shipping_address_2() {
		return $this->get_shipping_field_value( 'address_2' );
	}

	/**
	 * Get shipping postcode.
	 *
	 * @return string
	 */
	public function get_shipping_postcode() {
		return $this->get_shipping_field_value( 'postcode' );
	}

	/**
	 * Get shipping city.
	 *
	 * @return string
	 */
	public function get_shipping_city() {
		return $this->get_shipping_field_value( 'city' );
	}

	/**
	 * Get shipping state.
	 *
	 * @return string
	 */
	public function get_shipping_state() {
		return $this->get_shipping_field_value( 'state' );
	}

	/**
	 * Get shipping country.
	 *
	 * @return string
	 */
	public function get_shipping_country() {
		return $this->get_shipping_field_value( 'country' );
	}

}
