<?php

/**
 * Loads map with collection points.
 *
 * WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax_Request_Data
 */
class WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax_Request_Data extends WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data {

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax_Request_Data.
	 *
	 * @param array $request Request.
	 */
	public function __construct( $request ) {
		parent::__construct( $request );
		$this->post_data = $request;
	}


}
