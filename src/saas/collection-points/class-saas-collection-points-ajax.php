<?php

/**
 * Loads map with collection points.
 *
 * WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax
 */
class WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax {

	const STATUS_OK    = 'ok';
	const STATUS_ERROR = 'error';

	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	const NONCE_ACTION = 'fs_collection_points_map';

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS connection.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service
	) {
		$this->saas_connection  = $saas_connection;
		$this->shipping_service = $shipping_service;
	}

	/**
	 * Handle request.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points Service collection points.
	 * @throws RuntimeException Exception.
	 */
	public function handle_request( WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points ) {
		if ( check_ajax_referer( self::NONCE_ACTION, 'security', false ) ) {
			$response = array(
				'status' => self::STATUS_OK,
			);

			$request_data = new WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax_Request_Data( $_REQUEST );

			$address = new \WPDesk\SaasPlatformClient\Model\Shipment\Address();
			$address->setAddressLine1( $request_data->get_shipping_address_1() );
			$address->setAddressLine2( $request_data->get_shipping_address_2() );
			$address->setPostalCode( $request_data->get_shipping_postcode() );
			$address->setCity( $request_data->get_shipping_city() );
			$address->setStateCode( $request_data->get_shipping_state() );
			$address->setCountryCode( $request_data->get_shipping_country() );
			try {
				$collection_points  = $service_collection_points->get_nearest_collection_points( $address );
				$response['points'] = $collection_points->getPoints()->points;
			} catch ( Exception $e ) {
				$response = array(
					'status'  => self::STATUS_ERROR,
					'message' => $e->getMessage(),
				);
			}
		} else {
			$response = array(
				'status'  => self::STATUS_ERROR,
				'message' => __( 'Invalid nonce!', 'flexible-shipping' ),
			);
		}
		wp_send_json( $response );
	}
}
