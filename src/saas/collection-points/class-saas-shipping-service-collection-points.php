<?php

/**
 * Handles collection points .
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points implements \WPDesk\PluginBuilder\Plugin\Hookable {

	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS connection.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service
	) {
		$this->saas_connection  = $saas_connection;
		$this->shipping_service = $shipping_service;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		$ajax_action = 'fs_collection_points_' . $this->shipping_service->get_id();
		add_action( 'wp_ajax_' . $ajax_action, array( $this, 'handle_ajax_get_collection_points' ) );
		add_action( 'wp_ajax_nopriv_' . $ajax_action, array( $this, 'handle_ajax_get_collection_points' ) );
	}

	/**
	 * Handle AJAX.
	 */
	public function handle_ajax_get_collection_points() {
		$ajax_handler = new WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax( $this->saas_connection, $this->shipping_service );
		$ajax_handler->handle_request( $this );
	}

	/**
	 * Get nearest collection points.
	 *
	 * @param \WPDesk\SaasPlatformClient\Model\Shipment\Address $address Address.
	 * @return \WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints\PostSearchResponse
	 */
	public function get_nearest_collection_points( \WPDesk\SaasPlatformClient\Model\Shipment\Address $address ) {
		$platform       = $this->saas_connection->get_platform();
		$search_request = new WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SearchRequest();
		$search_request->setAddress( $address );
		$search_request->setRadius( 50 );
		return $platform->requestPostSearchParcelCollectionPoints( $search_request, $this->shipping_service->get_id() );
	}

	/**
	 * Prepare collection point options for address.
	 *
	 * @param \WPDesk\SaasPlatformClient\Model\Shipment\Address $address Address.
	 * @return array
	 */
	public function prepare_collection_points_options_for_address( \WPDesk\SaasPlatformClient\Model\Shipment\Address $address ) {
		$collection_points         = $this->get_nearest_collection_points( $address );
		$collection_points_options = array();
		if ( $collection_points->isCollectionPointError() ) {
			$collection_points_options[''] = __( 'Cannot get data for this address!', 'flexible-shipping' );
		} else {
			foreach ( $collection_points->getPoints()->points as $collection_point ) {
				$label  = $collection_point->getName();
				$label .= ', ' . $collection_point->getAddress()->getCity();
				$label .= ' ' . $collection_point->getAddress()->getPostalCode();
				$label .= ', ' . $collection_point->getAddress()->getAddressLine1();
				$label .= ' ' . $collection_point->getAddress()->getAddressLine2();

				$collection_points_options[ $collection_point->getId() ] = ucwords( strtolower( $label ) );
			}
		}
		return $collection_points_options;
	}

	/**
	 * Get collection point.
	 *
	 * @param string $collection_point_id Collection point id.
	 *
	 * @return mixed
	 */
	public function get_collection_point( $collection_point_id ) {
		$platform       = $this->saas_connection->get_platform();
		$single_request = new WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SingleRequest();
		$single_request->setId( $collection_point_id );
		return $platform->requestGetSingleCollectionPoint( $single_request, $this->shipping_service->get_id() );
	}

	/**
	 * Format single point description from response.
	 *
	 * @param \WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints\GetSingleResponse $response Response.
	 *
	 * @return string
	 */
	public function format_single_point_description_from_response( \WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints\GetSingleResponse $response ) {
		$description  = $response->getSingleResponse()->point->getName();
		$description .= ', ' . $response->getSingleResponse()->point->getAddress()->getAddressLine1();
		if ( ! empty( $response->getSingleResponse()->point->getAddress()->getAddressLine2() ) ) {
			$description .= ' ' . $response->getSingleResponse()->point->getAddress()->getAddressLine2();
		}
		$description .= ', ' . $response->getSingleResponse()->point->getAddress()->getPostalCode();
		$description .= ' ' . $response->getSingleResponse()->point->getAddress()->getCity();
		return $description;
	}

}
