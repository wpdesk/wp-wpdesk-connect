<!DOCTYPE html>
<html lang="pl">
<head>
	<title><?php _e('Select point', 'flexible-shipping'); ?></title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
	      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
	      crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
	        integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
	        crossorigin=""></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link href="https://unpkg.com/leaflet-geosearch@latest/assets/css/leaflet.css" rel="stylesheet"/>
	<script src="https://unpkg.com/leaflet-geosearch@latest/dist/bundle.min.js"></script>
	<script>
		var parent = window.opener;
		var ajax_url = parent.fs_collection_points_map.ajax_url;
		var loading_image = parent.fs_collection_points_map.loading_image;
		var marker_icon = parent.fs_collection_points_map.marker_icon;
		var lang = parent.fs_collection_points_map.lang;

		var OpenStreetMapProvider = window.GeoSearch.OpenStreetMapProvider;
		var provider = new OpenStreetMapProvider();
		var markers = [];
		var selectedMarker = null;

		$(document).ready(function () {
			var map = L.map('map');

			var userLocationIcon = L.icon({
				iconUrl: marker_icon,
				iconSize:     [25, 41],
			});
			window.opener.postMessage({action: 'get_adresses_data'}, window.location.origin);
			window.addEventListener('message', function (event) {
				if (event.origin === window.location.origin && 'send_address_data' === event.data.action) {
					var ajax_data = event.data.addresses_data;
					provider
						.search({query: get_shipping_address(ajax_data)})
						.then(function (result) {
							if (result[0].y && result[0].x) {
								var userMarker = L.marker([parseFloat(result[0].y), parseFloat(result[0].x)], {title: lang.user_location,  icon: userLocationIcon}).addTo(map);
								userMarker.bindPopup( lang.user_location );
								markers.push(userMarker);
							}
						});

					jQuery('#map').css('background-image', 'url(' + loading_image + ')');
					$.post(ajax_url, ajax_data, function (response) {
						if (response.points && 'ok' === response.status) {
							var points = response.points;
							var group = new L.featureGroup();
							if (map) {
								$('#map').css('background-image', 'none');
							}
							L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
								attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
							}).addTo(map);
							$.each(points, function (i, point) {
								var marker = L.marker([parseFloat(point.mapLocation.lat), parseFloat(point.mapLocation.lng)], {title: point.name});
								marker.bindPopup(popup_point_html(point));
								markers.push(marker);
								if (point.id === event.data.addresses_data.selected_point) {
									selectedMarker = marker;
								}
							});
							var group = L.featureGroup(markers).addTo(map);
							map.fitBounds(group.getBounds());
							if (null !== selectedMarker) {
								selectedMarker.fireEvent('click');
							}
						} else if ('error' === response.status) {
							alert(response.message);
							window.close();
						} else {
							alert(lang.no_points);
							window.close();
						}
					});
				}
			}, true);
		});

		function get_shipping_address(data) {
			var address, city, postcode;
			if (data.different_addres === true) {
				address = data.shipping_address_1;
				city = data.shipping_city;
				postcode = data.shipping_postcode;
			} else {
				address = data.billing_address_1;
				city = data.billing_city;
				postcode = data.billing_postcode;
			}
			var new_address = address.split('/');
			if( new_address[0] ) {
				address = new_address[0];
			}
			return address + ', ' + postcode + ', ' + city;
		}

		function popup_point_html(point) {
			if (point.address.addressLine2 === null) {
				point.address.addressLine2 = '';
			} else {
				point.address.addressLine2 = '<br/>' + point.address.addressLine2;
			}
			var output = '<div class="point-popup">';
			output += '<h4>' + point.name + '</h4>';
			output += '<p>' + point.address.addressLine1 + point.address.addressLine2 + '</p>';
			output += '<p>' + point.address.postalCode + '</p>';
			output += '<p>' + point.address.city + '</p>';
			output += '<p>' + point.description + '</p>';
			output += '<p class="submit-button"><a href="javascript:select_point( \'' + point.id + '\' )"><?php _e('Select', 'flexible-shipping'); ?></a></p>';
			output += '</div>';
			return output;
		}

		function select_point(id) {
			window.opener.postMessage({action: 'select_point', point_id: id}, window.location.origin);
			window.close();
		}
	</script>
	<style>
		html, body {
			height: 100%;
			margin: 0;
		}

		#map {
			width: 100%;
			height: 100%;
			background-color: #EEE;
			background-repeat: no-repeat;
			background-position: center;
		}

		.leaflet-popup-content-wrapper {
			border-radius: 2px;
		}

		.point-popup p {
			margin: 0 0 5px;
			padding: 0 0;
		}

		.submit-button a {
			display: inline-block;
			text-decoration: none;
			font-size: 13px;
			line-height: 26px;
			height: 28px;
			margin: 0;
			padding: 0 10px 1px;
			cursor: pointer;
			border-width: 1px;
			border-style: solid;
			-webkit-appearance: none;
			border-radius: 3px;
			white-space: nowrap;
			box-sizing: border-box;
			background: #0085ba;
			border-color: #0073aa #006799 #006799;
			box-shadow: 0 1px 0 #006799;
			color: #fff;
		}
	</style>
</head>
<body>
<div id='map'></div>
</body>
</html>
