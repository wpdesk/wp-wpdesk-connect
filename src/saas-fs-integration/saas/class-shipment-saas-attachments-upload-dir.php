<?php

/**
 * Handle SaaS attachments upload dir.
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Attachments_Upload_Dir implements \WPDesk\PluginBuilder\Plugin\Hookable {

	const UPLOADS_PATH = '/woocommerce_uploads/shipment_attachments';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'upload_dir', array( $this, 'upload_dir' ) );
	}

	/**
	 * Change upload dir for downloadable files.
	 *
	 * @param array $path_data Array of paths.
	 *
	 * @return array
	 */
	public function upload_dir( $path_data ) {
		if ( isset( $_POST['type'] ) && WPDesk_Flexible_Shipping_Shipment_CPT::POST_TYPE_SHIPMENT === $_POST['type'] ) { // WPCS: CSRF ok, input var ok.

			if ( empty( $path_data['subdir'] ) ) {
				$path_data['path']   = $path_data['path'] . self::UPLOADS_PATH;
				$path_data['url']    = $path_data['url'] . self::UPLOADS_PATH;
				$path_data['subdir'] = self::UPLOADS_PATH;
			} else {
				$new_subdir = self::UPLOADS_PATH . $path_data['subdir'];

				$path_data['path']   = str_replace( $path_data['subdir'], $new_subdir, $path_data['path'] );
				$path_data['url']    = str_replace( $path_data['subdir'], $new_subdir, $path_data['url'] );
				$path_data['subdir'] = str_replace( $path_data['subdir'], $new_subdir, $path_data['subdir'] );
			}
		}
		return $path_data;
	}

}
