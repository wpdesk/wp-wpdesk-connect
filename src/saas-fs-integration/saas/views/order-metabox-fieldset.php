<?php
/**
 * Display fieldset in metabox.
 *
 * @var array $fields
 * @var array $values
 * @var bool  $disabled
 */
?>
<?php foreach ( $fields as $field ) : ?>
	<?php
	if ( ! empty( $field['id'] ) ) {
		$key  = $field['id'];
		$args = array(
			'label' => do_shortcode( $field['name'] ),
			'id'    => $key . '_' . $id,
			'type'  => $field['type'],
		);
		if ( 'select' === $field['type'] ) {
			$args['options'] = array();
			foreach ( $field['select-additional']['options'] as $option ) {
				$args['options'][ $option['value'] ] = $option['text'];
			}
		}
		if ( 'parcel_collection_points' === $field['type'] ) {
			$args['options'] = $field['access_points_options'];
			$args['class']   = 'fs_select2';
		}
		if ( isset( $values[ $key ] ) ) {
			$args['value'] = $values[ $key ][0];
		}
		if ( $disabled ) {
			$args['custom_attributes'] = array( 'disabled' => 'disabled' );
		}
		if ( isset( $field['desc'] ) ) {
			$args['description'] = do_shortcode( $field['desc'] );
		}
		if ( isset( $field['tooltip'] ) && is_array( $field['tooltip'] ) ) {
			$args['description'] = do_shortcode( $field['tooltip']['description'] );
			$args['desc_tip']    = true;
		}
	}
	?>
	<div class="<?php echo esc_attr( $key ); ?>">
		<?php if ( 'checkbox' === $field['type'] ) : ?>
			<?php $args['label'] .= ' '; ?>
			<?php woocommerce_wp_checkbox( $args ); ?>
		<?php elseif ( 'header' === $field['type'] ) : ?>
			<h4><?php echo $field['name']; /* phpcs:ignore */ ?></h4>
		<?php elseif ( 'select' === $field['type'] || 'parcel_collection_points' === $field['type'] ) : ?>
			<?php woocommerce_wp_select( $args ); ?>
		<?php else : ?>
			<?php woocommerce_wp_text_input( $args ); ?>
		<?php endif; ?>
		<?php if ( isset( $field['visible-when'] ) ) : ?>
			<script type="text/javascript">
				new ApConditionalLogic( '<?php echo esc_attr( $field['id'] ); ?>', <?php echo esc_attr( $id ); ?>, <?php echo wp_json_encode( $field['visible-when'] ); ?> );
			</script>
		<?php endif; ?>
	</div>
<?php endforeach; ?>
