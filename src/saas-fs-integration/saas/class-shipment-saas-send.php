<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Saas_Send
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Send {

	const WEIGHT_UNIT       = 'G';
	const WOOCOMMERCE_GRAMS = 'g';

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Send constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function __construct( WPDesk_Flexible_Shipping_Shipment_Saas $shipment ) {
		$this->shipment = $shipment;
	}

	/**
	 * Prepare ship to address.
	 *
	 * @param WC_Order $order Order.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\Address
	 */
	private function prepare_ship_to_address( $order ) {
		$ship_to_address = new \WPDesk\SaasPlatformClient\Model\Shipment\Address();
		$ship_to_address->setAddressLine1( $order->get_shipping_address_1() );
		$ship_to_address->setAddressLine2( $order->get_shipping_address_2() );
		$ship_to_address->setPostalCode( str_replace( '-', '', $order->get_shipping_postcode() ) );
		$ship_to_address->setCity( $order->get_shipping_city() );
		$ship_to_address->setCountryCode( $order->get_shipping_country() );
		$ship_to_address->setStateCode( $order->get_shipping_state() );

		return $ship_to_address;
	}

	/**
	 * Prepare ship to actor.
	 *
	 * @param WC_Order $order Order.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\Actor
	 */
	private function prepare_ship_to( $order ) {
		$ship_to = new \WPDesk\SaasPlatformClient\Model\Shipment\Actor();
		$ship_to->setAddress( $this->prepare_ship_to_address( $order ) );
		$ship_to->setName( trim( $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name() ) );
		$ship_to->setCompanyName( $order->get_shipping_company() );
		$ship_to->setPhoneNumber( $order->get_billing_phone() );
		$ship_to->setEmail( $order->get_billing_email() );

		return $ship_to;
	}

	/**
	 * Prepare packages.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\Package[]
	 */
	private function prepare_packages() {
		$package = new \WPDesk\SaasPlatformClient\Model\Shipment\Package();
		$package->seTtype( $this->shipment->get_meta( 'package_type' ) );
		$package->setWeight( intval( wc_get_weight( floatval( $this->shipment->get_meta( 'package_weight' ) ), self::WOOCOMMERCE_GRAMS ) ) );
		$package->setDescription( $this->shipment->get_meta( 'description' ) );

		$package->dimensions = new \WPDesk\SaasPlatformClient\Model\Shipment\Package\Dimensions();
		$package->dimensions->setHeight( intval( wc_get_dimension( floatval( $this->shipment->get_meta( 'height' ) ), 'mm' ) ) );
		$package->dimensions->setWidth( intval( wc_get_dimension( floatval( $this->shipment->get_meta( 'width' ) ), 'mm' ) ) );
		$package->dimensions->setLength( intval( wc_get_dimension( floatval( $this->shipment->get_meta( 'length' ) ), 'mm' ) ) );

		$packages = array( $package );

		return $packages;
	}

	/**
	 * Prepare commodities.
	 *
	 * @param WC_Order $order Order.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\ClientCommodity[]
	 */
	private function prepare_commodities( $order ) {
		$commodities = array();
		/** @var WC_Order_Item_Product $order_item */
		foreach ( $order->get_items() as $order_item ) {
			$unit_price           = new \WPDesk\SaasPlatformClient\Model\Shipment\ClientMoney();
			$unit_price->currency = $order->get_currency();
			$total_order_item     = floatval( $order_item->get_total() ) + floatval( $order_item->get_total_tax() );
			$unit_price->amount   = strval( round( $total_order_item / floatval( $order_item->get_quantity() ), wc_get_rounding_precision() ) );

			$unit_weight = new \WPDesk\SaasPlatformClient\Model\Shipment\ClientWeight();
			$unit_weight->setUnit( self::WEIGHT_UNIT );
			$unit_weight->setAmount( intval( wc_get_weight( $order_item->get_product()->get_weight(), self::WOOCOMMERCE_GRAMS ) ) );

			$description    = $order_item->get_product()->get_attribute( 'shipping-desc' );
			$origin_country = $order_item->get_product()->get_attribute( 'origin-country' );
			$hts_code       = $order_item->get_product()->get_attribute( 'hts-code' );

			if ( ! empty( $description ) && ! empty( $origin_country ) && ! empty( $hts_code ) ) {
				$commodities[] = new \WPDesk\SaasPlatformClient\Model\Shipment\ClientCommodity(
					$order_item->get_name(),
					$unit_price,
					$unit_weight,
					$order_item->get_quantity(),
					$description,
					$origin_country,
					$hts_code
				);
			}
		}
		return $commodities;
	}

	/**
	 * Prepare shop data.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\ShopData
	 */
	private function prepare_shop_data() {
		$shop_data = new \WPDesk\SaasPlatformClient\Model\Shipment\ShopData();
		$shop_data->setOrderId( $this->shipment->get_order_id() );
		$shop_data->setShipmentId( $this->shipment->get_id() );
		return $shop_data;
	}

	/**
	 * Prepare shop flow.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\ShopFlow
	 */
	private function prepare_shop_flow() {
		$shipment  = $this->shipment;
		$shop_flow = new \WPDesk\SaasPlatformClient\Model\Shipment\ShopFlow();
		$shop_flow->setCreatedVia( $shipment->get_created_via() );
		$shop_flow->setSentVia( $shipment->get_sent_via() );
		$shop_flow->setFallback( $shipment->was_fallback() );
		$shop_flow->setFallbackMessage( $shipment->get_fallback_message() );
		$shop_flow->setRateType( $shipment->get_rate_type() );
		$shop_flow->setCheckoutServiceType( $shipment->get_checkout_service_type() );
		$shop_flow->setCheckoutServiceName( $shipment->get_checkout_service_name() );
		return $shop_flow;
	}

	/**
	 * Prepare shipment request.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest
	 */
	private function prepare_shipment_request() {

		$shipment_request = new \WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest();
		$shipment_request->setShipTo( $this->prepare_ship_to( $this->shipment->get_order() ) );
		$shipment_request->setPackages( $this->prepare_packages() );

		$shipment_request->setCommodities( $this->prepare_commodities( $this->shipment->get_order() ) );

		$shipment_request->setServiceType( $this->shipment->get_meta( 'service_type' ) );

		$shipment_request->setShopData( $this->prepare_shop_data() );
		$shipment_request->setShopFlow( $this->prepare_shop_flow() );

		$shipment_request_fields = new WPDesk_Flexible_Shipping_Shipment_Request_Fields( $this->shipment, $shipment_request );
		$shipment_request_fields->prepare_shipment_fields();

		return $shipment_request;
	}

	/**
	 * Send shipment to platform.
	 *
	 * @return WPDesk\SaasPlatformClient\Model\Shipment\ShipmentResponse
	 * @throws WPDesk_Flexible_Shipping_Send_Shipment_Exception Send Shipment Exception.
	 */
	public function send() {
		$shipment         = $this->shipment;
		$platform         = $shipment->get_platform();
		$shipment_request = $this->prepare_shipment_request();
		$response         = $platform->requestPostShipment( $shipment_request, $shipment->get_shipping_method()->get_shipping_service_id() );
		if ( $response->isError() ) {
			$error_body = $response->getResponseErrorBody();
			$message    = __( 'UPS! Something is wrong!', 'flexible-shipping' );

			if ( isset( $error_body['detail'] ) ) {
				$message = $error_body['detail'];
			}
			if ( isset( $error_body['message'] ) ) {
				$message = $error_body['message'];
			}
			$shipment->set_meta( $shipment::META_SAAS_MESSAGE, $message );
			$shipment->update_status( 'fs-failed' );
			$shipment->save();
			if ( $response->isShipmentPlanExceeded() ) {
				throw new WPDesk_Flexible_Shipping_Shipment_Plan_Exceeded_Exception( $shipment->get_error_message(), $response->getResponseCode() );
			} else {
				throw new WPDesk_Flexible_Shipping_Send_Shipment_Exception( $shipment->get_error_message(), $response->getResponseCode() );
			}
		}

		$shipment_response = $response->getShipment();
		$shipment->set_saas_shipment_id( $shipment_response->getShipmentId() );
		$shipment->set_tracking_number( $shipment_response->getTrackingId() );
		$shipment->set_tracking_url( $shipment_response->getTrackingUrl() );
		if ($shipment_response->hasShipmentCost()) {
			$shipment->set_shipment_cost( $shipment_response->getShipmentCost() );
		}
		$shipment->delete_meta( $shipment::META_SAAS_MESSAGE );
		$shipment->update_status( 'fs-' . $shipment_response->getStatus() );
		$shipment->save();

		$shipment->add_order_note_shipment_created();

		do_action( 'flexible_shipping_shipment_confirmed', $shipment );

		return $shipment_response;
	}

}
