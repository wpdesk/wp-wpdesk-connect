<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Saas_Ajax
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Ajax {

	const META_SAAS_MESSAGE = '_saas_message';

	const UPGRADE_PLAN_URL = 'https://www.flexibleshipping.com/connect/purchase/';

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * Links.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
	 */
	private $saas_platform_links;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Ajax constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas       $shipment Shipment.
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Platform links.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_Shipment_Saas $shipment,
		WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links
	) {
		$this->shipment            = $shipment;
		$this->saas_platform_links = $saas_platform_links;
	}

	/**
	 * Save field data.
	 *
	 * @param array $field Field.
	 * @param array $data Data.
	 */
	private function save_field_data( array $field, array $data ) {
		$id         = $this->shipment->get_id();
		$meta_value = '';
		$data_name  = $field['id'] . '_' . $id;
		if ( isset( $data[ $data_name ] ) ) {
			$meta_value = $data[ $data_name ];
		}
		$this->shipment->set_meta( $field['id'], $meta_value );
	}

	/**
	 * Save fieldset data.
	 *
	 * @param array $fieldset Fieldset.
	 * @param array $data Data.
	 */
	private function save_fieldset_data( array $fieldset, array $data ) {
		foreach ( $fieldset['fields'] as $field ) {
			$this->save_field_data( $field, $data );
		}
	}

	/**
	 * Save data from metabox.
	 *
	 * @param array $data Data.
	 */
	private function save_ajax_data( array $data ) {
		$fields = $this->shipment->get_fields_for_targets();
		foreach ( $fields as $fieldset ) {
			if ( isset( $fieldset['show-in'] ) && in_array( 'metabox', $fieldset['show-in'], true ) ) {
				$this->save_fieldset_data( $fieldset, $data );
			}
		}
		$id         = $this->shipment->get_id();
		$data_name  = 'attachments_' . $id;
		$meta_value = '';
		if ( isset( $data[ $data_name ] ) ) {
			$meta_value = $data[ $data_name ];
			unset( $meta_value['{count}'] );
		}
		$this->shipment->set_meta( 'attachments', $meta_value );
	}

	/**
	 * Shipment exceeded message.
	 *
	 * @return string
	 */
	private function shipments_exceeded_message() {
		return sprintf(
			// Translators: link.
			__( '%1$s Shipment plan exceeded. %2$sPlease upgrade your plan →%3$s', 'flexible-shipping' ),
			'</p><p class="fs-saas-no-labels-left">',
			'<a target="_blank" href="' . $this->saas_platform_links->add_utm(
				$this->saas_platform_links->get_upgrade_plan(),
				'fs-notice-myaccount',
				'flexible-shipping',
				'link',
				'user-site',
				''
			) . '" target="blank">',
			'</a>'
		);
	}

	/**
	 * Save metabox data.
	 *
	 * @param array $data Data.
	 * @param array $response Response.
	 *
	 * @return mixed
	 * @throws RuntimeException Exception.
	 */
	private function action_save( array $data, array $response ) {
		$metabox_can_be_saved = $this->shipment->matabox_can_be_saved();
		if ( ! $metabox_can_be_saved ) {
			throw new RuntimeException( __( 'Shipment cannot be saved for this status!', 'flexible-shipping' ) );
		}
		$this->save_ajax_data( $data );
		$this->shipment->set_meta( self::META_SAAS_MESSAGE, '' );
		$this->shipment->update_status( 'fs-new' );
		$this->shipment->save();
		$response['status']  = 'success';
		$response['message'] = __( 'Saved', 'flexible-shipping' );

		return $response;
	}

	/**
	 * Action send.
	 *
	 * @param array $response Response.
	 *
	 * @return mixed
	 */
	private function action_send( array $response ) {
		try {
			$shipment = $this->shipment;
			$shipment->set_sent_via_metabox();
			$shipment_response   = $this->shipment->send_shipment();
			$shipment_left       = intval( $shipment_response->getShipmentCountLeft() );
			$shipment_sent       = intval( $shipment_response->getShipmentCountSent() );
			$response['message'] = sprintf(
				// Translators: shipments counts (sent/all).
				__( 'Shipment created.%1$sFS Connect plan usage: %2$s/%3$s shipments', 'flexible-shipping' ),
				'<br />',
				$shipment_sent,
				$shipment_left + $shipment_sent
			);
			if ( 0 === $shipment_left ) {
				$response['message'] .= $this->shipments_exceeded_message();
			}
		} catch ( WPDesk_Flexible_Shipping_Shipment_Plan_Exceeded_Exception $e ) {
			$response['status']  = 'fail';
			$response['message'] = $this->shipments_exceeded_message();
		} catch ( Exception $e ) {
			$response['status']  = 'fail';
			$response['message'] = $e->getMessage();
		}

		return $response;
	}

	/**
	 * Handle AJAX requests for shipment.
	 *
	 * @param string $action Action.
	 * @param array  $data Data.
	 *
	 * @return array
	 *
	 * @throws WPDesk_Flexible_Shipping_Send_Shipment_Exception Send Shipment Exception.
	 * @throws RuntimeException Runtime Exception.
	 */
	public function ajax_request( $action, $data ) {
		$response = array();
		if ( 'save' === $action || 'send' === $action ) {
			$response = $this->action_save( $data, $response );
		}
		if ( 'send' === $action ) {
			$response = $this->action_send( $response );
		}
		if ( 'cancel' === $action ) {
			try {
				$this->shipment->cancel_shipment();
				$response['message'] = __( 'Canceled', 'flexible-shipping' );
			} catch ( Exception $e ) {
				$response['status']  = 'fail';
				$message_formatter   = new WPDesk_Flexible_Shipping_Shipment_Saas_Message_Formatter(
					$e->getMessage(),
					$this->shipment->get_shipping_service_name()
				);
				$response['message'] = $message_formatter->format_to_html();
			}
		}

		$response['content'] = $this->shipment->get_order_metabox_content();
		return $response;
	}

}
