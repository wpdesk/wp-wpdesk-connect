## [1.1.1] - 2019-07-19
### Added
- Remove from Flexible Shipping when not connected to platform

## [1.1.0] - 2019-06-17
### Added
- Refactor so the platform factory can be injected

## [1.0.0] - 2019-06-17
### Added
- Init