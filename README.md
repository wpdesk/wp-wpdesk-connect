[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-connect/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-connect/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-connect/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-connect/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-connect/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-connect/commits/master)

wp-wpdesk-connect
====================
